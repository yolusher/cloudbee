<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});



Route::group(['prefix' => 'auth'], function () {
    Route::post('login', 'AuthController@login');
    Route::post('signup', 'AuthController@signup');
    Route::group([
      'middleware' => 'auth:api'
    ], function() {
        Route::get('logout', 'AuthController@logout');
        Route::get('user', 'AuthController@user');
    });
});

Route::group(['middleware' => 'auth:api'], function() {
    Route::resource('/{company}/queen', 'c_queen_bee')->middleware('cors');
    Route::get('/{company}/cb/queen', 'c_queen_bee@combobox')->middleware('cors');

    Route::resource('/{company}/hives_origin', 'c_hives_origin')->middleware('cors');
    Route::get('/{company}/cb/hives_origin', 'c_hives_origin@combobox')->middleware('cors');

    Route::resource('/{company}/hives_type', 'c_hives_type')->middleware('cors');
    Route::get('/{company}/cb/hives_type', 'c_hives_type@combobox')->middleware('cors');

    Route::resource('/{company}/apiaries', 'c_apiaries')->middleware('cors');
    Route::get('/{company}/cb/apiaries', 'c_apiaries@combobox')->middleware('cors');

    Route::resource('/{company}/hives', 'c_hives')->middleware('cors');
    Route::get('/{company}/gc/hives', 'c_hives@getcode')->middleware('cors');

    Route::resource('/{company}/illness', 'c_illness')->middleware('cors');
    Route::get('/{company}/cb/illness', 'c_illness@combobox')->middleware('cors');

    Route::resource('/{company}/problem', 'c_problem')->middleware('cors');
    Route::get('/{company}/cb/problem', 'c_problem@combobox')->middleware('cors');

    Route::resource('/{company}/treatment', 'c_treatment')->middleware('cors');
    Route::get('/{company}/cb/treatment', 'c_treatment@combobox')->middleware('cors');
    Route::get('/{company}/treatment/illness/{code}/', 'c_treatment@getbyillness')->middleware('cors');

    //Route::resource('/{company}/equipment', 'c_equipment')->middleware('cors');
    Route::get('/{company}/equipment/hives/{hives}/', 'c_equipment@showequipment')->middleware('cors');
    Route::get('/{company}/cb/equipment', 'c_equipment@combobox')->middleware('cors');

    Route::resource('/{company}/storage', 'c_storage')->middleware('cors');
    

    Route::resource('/{company}/meekness', 'c_meekness')->middleware('cors');
   

    Route::resource('/{company}/unit_measure', 'c_unit_measure')->middleware('cors');
    Route::get('/{company}/cb/unit_measure', 'c_unit_measure@combobox')->middleware('cors');

    Route::resource('/{company}/global', 'c_global')->middleware('cors');
    Route::get('/{company}/cb/global', 'c_global@combobox')->middleware('cors');


    
    Route::get('/{company}/cb/elements', 'c_elements@combobox')->middleware('cors');

    
    Route::get('/{company}/cb/production', 'c_production@combobox')->middleware('cors');

    Route::resource('/{company}/supplier', 'c_supplier')->middleware('cors');
    Route::get('/{company}/cb/supplier', 'c_supplier@combobox')->middleware('cors');
    Route::get('/{company}/cb/supplier/{code}', 'c_supplier@comboboxcode')->middleware('cors'); 

    Route::resource('/{company}/mother', 'c_mother')->middleware('cors');
    
    Route::get('/{company}/gc/mother', 'c_mother@getcode')->middleware('cors');

    Route::resource('/{company}/father', 'c_father')->middleware('cors');
    Route::get('/{company}/cb/father', 'c_father@combobox')->middleware('cors');
    Route::get('/{company}/gc/father', 'c_father@getcode')->middleware('cors');
    Route::get('/{company}/father/mother/{father}', 'c_father@show_mother')->middleware('cors');

    Route::resource('/{company}/children', 'c_children')->middleware('cors');
    Route::get('/{company}/cb/children', 'c_children@combobox')->middleware('cors');
    Route::get('/{company}/cbna/children', 'c_children@comboboxnoassig')->middleware('cors');

    Route::resource('/{company}/calification', 'c_calification')->middleware('cors');
    Route::get('/{company}/cb/calification', 'c_calification@combobox')->middleware('cors');

    
    Route::resource('/{company}/situation', 'c_situation')->middleware('cors');
    Route::get('/{company}/cb/situation', 'c_situation@combobox')->middleware('cors');

    Route::resource('/{company}/comp', 'c_company')->middleware('cors');
    Route::resource('/{company}/usertype', 'c_usertype')->middleware('cors');
    Route::resource('/{company}/users', 'c_user')->middleware('cors');

});

Route::get('/{company}/getform', 'c_getform@getform')->middleware('cors');
Route::get('/{company}/gethivesapp', 'c_hives@hiveservice')->middleware('cors');
Route::get('/{company}/getapiaryapp', 'c_apiaries@apiaryservice')->middleware('cors');

Route::resource('/{company}/result', 'c_result')->middleware('cors');
Route::get('/{company}/cb/result/{hives}', 'c_result@get_cb')->middleware('cors');
Route::post('/{company}/result/report', 'c_result@report')->middleware('cors');

Route::resource('/{company}/equipment', 'c_equipment')->middleware('cors');

Route::resource('/{company}/userapp', 'c_userapp')->middleware('cors');
Route::post('/userapp/login', 'c_userapp@login')->middleware('cors');
Route::resource('/{company}/status', 'c_status')->middleware('cors');
Route::get('/{company}/cb/status', 'c_status@combobox')->middleware('cors');
Route::resource('/{company}/task', 'c_task')->middleware('cors');
Route::get('/{company}/cb/task', 'c_task@combobox')->middleware('cors');
Route::get('/{company}/task/problem/{code}/', 'c_task@getbyproblem')->middleware('cors');

Route::resource('/company', 'c_company')->middleware('cors');

Route::resource('/plans', 'c_plans')->middleware('cors');
Route::get('/cb/plans', 'c_plans@combobox')->middleware('cors');

Route::get('file/company_pay/{company}/{name}', 'f_paymentfile@getfile');
Route::post('file/company_pay/', 'f_paymentfile@savefile');

Route::get('/{company}/cb/storage', 'c_storage@combobox')->middleware('cors');


Route::get('/{company}/cb/hives', 'c_hives@combobox')->middleware('cors');
Route::get('/{company}/cb/mother', 'c_mother@combobox')->middleware('cors');

Route::get('/{company}/cb/meekness', 'c_meekness@combobox')->middleware('cors');


Route::resource('/{company}/elements', 'c_elements')->middleware('cors');

Route::resource('/{company}/production', 'c_production')->middleware('cors');
