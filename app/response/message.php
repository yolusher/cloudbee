<?php

namespace App\response;

class message 
{
	//mensaje
	public function create(){
		return "Registro agregado";
	}

	public function delete(){
		return "Registro eliminado";
	}

	public function update(){
		return "Registro actualizado";
	}

	public function NotFound(){
		return "Registro no encontrado";
	}
	public function duplicate(){
		return "Código duplicado";
	}

	// color
	public function error(){
		return "danger";
	}
	public function success(){
		return "success";
	}
}
