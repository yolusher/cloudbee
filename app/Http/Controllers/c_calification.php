<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\model\m_calification;
use App\response\message;

class c_calification extends Controller
{
    private $m;

    public function __construct()
    {
        $this->m = new message;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($company)
    {
        //
        $calification = m_calification::where('company', $company)->get()->toJson(JSON_PRETTY_PRINT);
        return response($calification, 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request,$company)
    {
        //
        if(m_calification::where('code', $request->code)->exists()){
            return response()->json(["message" => $this->m->duplicate(),"color" => $this->m->error()], 201);
        }
        $calification = new m_calification;
        $calification->code = $request->code;
        $calification->name = $request->name;
        $calification->company = $request->company;
        $calification->state = $request->state;
        $calification->save();
        return response()->json([
          "message" =>  $this->m->create(),"color" => $this->m->success()
        ], 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($company,$id)
    {
        //
        if (m_calification::where('id', $id)->exists()) {
            $calification = m_calification::where('id', $id)->where('company', $id)->get()->toJson(JSON_PRETTY_PRINT);
            return response($calification, 200);
        } else {
            return response()->json([
              "message" => $this->m->NotFound(),"color" => $this->m->error()
            ], 404);
        }
        return m_calification::where('id',$id)->get();
    }
    public function combobox($company)
    {
        $calification = m_calification::where('company', $company)->where('state', true)->select('code','name')->get()->toJson(JSON_PRETTY_PRINT);
        return response($calification, 200);
       
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$company,$id)
    {
        //
        if (m_calification::where('id', $id)->exists()) {
            $calification = m_calification::find($id);
            $calification->code = is_null($request->code) ? $calification->code : $request->code;
            $calification->name = is_null($request->name) ? $calification->name : $request->name;
            $calification->state = is_null($request->state) ? $calification->state : $request->state;
            $calification->company = is_null($request->company) ? $calification->company : $request->company;
            $calification->save();
            return response()->json([
                "message" => $this->m->update(),"color" => $this->m->success()
            ], 200);
        } else {
            return response()->json([
                "message" => $this->m->NotFound(),"color" => $this->m->error()
            ], 404);
            
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($company,$id)
    {
        //
        if(m_calification::where('id', $id)->exists()) {
            $calification = m_calification::find($id);
            $calification->delete();

            return response()->json([
              "message" => $this->m->delete(),"color" => $this->m->success()
            ], 202);
          } else {
            return response()->json([
              "message" => $this->m->NotFound(),"color" => $this->m->error()
            ], 404);
          }
    }
}
