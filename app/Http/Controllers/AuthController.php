<?php

namespace App\Http\Controllers;
use Mail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\model\m_company;
use Carbon\Carbon;
use App\User;
use Illuminate\Support\Facades\DB;
use App\response\message;
use App\Mail\CompanyAccept;


class AuthController extends Controller
{
    private $m;

    public function __construct()
    {
        $this->m = new message;
    }
    /**
     * Create user
     *
     * @param  [string] name
     * @param  [string] email
     * @param  [string] password
     * @param  [string] password_confirmation
     * @return [string] message
     */
    public function signup(Request $request)
    {
        $company = new m_company();
        $company->code = $request->companyCode;
        $company->name = $request->companyName;
        $company->plan = $request->plan;
        $company->state = 0;
        $company->save();
        $user = new User([
            'name' => $request->name,
            'username' => $request->email,
            'email' => $request->email,
            'password' => bcrypt($request->password),
            'role' => $request->role,
            'company' => $request->companyCode,
        ]);
        //mtpyldwoblspaivu "Su empresa necesita ser validad por un administrador"
        //Mail::to($request->email)->send();
        /*
        try {
            Mail::send(view('email_company', ['name' => 'James']), ['user' => $user], function ($m) use ($user) {
                $m->from('cloudbeepruebas@gmail.com', 'CloudBee');

                $m->to($user->email, $user->name)->subject('');
            });
        } catch (Throwable $e) {
            echo "Error de envio";
        }*/
        
        $user->save();
        DB::table('mst_bee_status')->insert(array('code'=>'000', 'name'=>'COMPRADA','company'=>$request->companyCode,'state'=>1));  
        DB::table('mst_bee_status')->insert(array('code'=>'001', 'name'=>'FECUNDADA','company'=>$request->companyCode,'state'=>1));  
        DB::table('mst_bee_status')->insert(array('code'=>'002', 'name'=>'VIRGEN','company'=>$request->companyCode,'state'=>1)); 
        
        DB::table('mst_bee_situation')->insert(array('code'=>'000', 'name'=>'MUERTA','company'=>$request->companyCode,'state'=>1));  
        DB::table('mst_bee_situation')->insert(array('code'=>'001', 'name'=>'ASIGNADA','company'=>$request->companyCode,'state'=>1));  
        DB::table('mst_bee_situation')->insert(array('code'=>'002', 'name'=>'SIN ASIGNAR','company'=>$request->companyCode,'state'=>1));

        DB::table('mst_ev_calification')->insert(array('code'=>'001', 'name'=>'ALTO','company'=>$request->companyCode,'state'=>1));  
        DB::table('mst_ev_calification')->insert(array('code'=>'002', 'name'=>'MEDIO','company'=>$request->companyCode,'state'=>1));  
        DB::table('mst_ev_calification')->insert(array('code'=>'003', 'name'=>'BAJO','company'=>$request->companyCode,'state'=>1));
        
        return response()->json([
            "message" =>  $this->m->create(),"color" => $this->m->success()
        ], 201);
    }
  
    /**
     * Login user and create token
     *
     * @param  [string] email
     * @param  [string] password
     * @param  [boolean] remember_me
     * @return [string] access_token
     * @return [string] token_type
     * @return [string] expires_at
     */
    public function login(Request $request)
    {
        $request->validate([
            'email' => 'required|string|email',
            'password' => 'required|string',
            'remember_me' => 'boolean'
        ]);        
        $credentials = request(['email', 'password']);        
        if(!Auth::attempt($credentials))
            return response()->json([
                'message' => 'Unauthorized'
            ], 401);        
        $user = $request->user();        
        $tokenResult = $user->createToken('Personal Access Token');
        $token = $tokenResult->token;        
        if ($request->remember_me)
            $token->expires_at = Carbon::now()->addWeeks(1);        
            $token->save();        
            return response()->json([
            'access_token' => $tokenResult->accessToken,
            'token_type' => 'Bearer',
            'expires_at' => Carbon::parse( $tokenResult->token->expires_at)->toDateTimeString(),
            'userData' => $request->user()
        ]);
    }
    /**
     * Logout user (Revoke the token)
     *
     * @return [string] message
     */
    public function logout(Request $request)
    {
        $request->user()->token()->revoke();        
        return response()->json([
            'message' => 'Successfully logged out'
        ]);
    }
  
    /**
     * Get the authenticated User
     *
     * @return [json] user object
     */
    public function user(Request $request)
    {
        return response()->json($request->user());
    }
}