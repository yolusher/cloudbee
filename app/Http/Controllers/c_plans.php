<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\model\m_plans;
use App\response\message;


class c_plans extends Controller
{ 
    private $m;

    public function __construct()
    {
        $this->m = new message;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $_plans = m_plans::get()->toJson(JSON_PRETTY_PRINT);
        return response($_plans, 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if(m_plans::where('code', $request->code)->exists()){
            return response()->json(["message" => $this->m->duplicate(),"color" => $this->m->error()], 201);
        }
        $_plans = new m_plans;
        $_plans->code = $request->code;
        $_plans->name = $request->name;
        $_plans->max_hives = $request->max_hives;
        $_plans->state = $request->state;
        $_plans->save();
        return response()->json([
          "message" =>  $this->m->create(),"color" => $this->m->success()
        ], 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if (m_plans::where('code', $id)->exists()) {
            $_plans = m_plans::where('code', $id)->get()->toJson(JSON_PRETTY_PRINT);
            return response($_plans, 200);
        } else {
            return response()->json([
              "message" => $this->m->NotFound(),"color" => $this->m->error()
            ], 404);
        }
        return m_plans::where('code',$id)->get();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }
    public function combobox()
    {
        $_plans = m_plans::where('state', true)->select('code','name','max_hives')->get()->toJson(JSON_PRETTY_PRINT);
        return response($_plans, 200);
       
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if (m_plans::where('code', $code)->exists()) {
            $_plans = m_plans::find($id);
            $_plans->code = is_null($request->code) ? $_plans->code : $request->code;
            $_plans->name = is_null($request->name) ? $_plans->name : $request->name;
            $_plans->max_hives = is_null($request->max_hives) ? $_plans->max_hives : $request->max_hives;
            $_plans->state = is_null($request->state) ? $_plans->state : $request->state;
            $_plans->save();
            return response()->json([
                "message" => $this->m->update(),"color" => $this->m->success()
            ], 200);
        } else {
            return response()->json([
                "message" => $this->m->NotFound(),"color" => $this->m->error()
            ], 404);
        
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if(m_plans::where('id', $id)->exists()) {
            $_plans = m_plans::find($id);
            $_plans->delete();

            return response()->json([
              "message" => $this->m->delete(),"color" => $this->m->success()
            ], 202);
          } else {
            return response()->json([
              "message" => $this->m->NotFound(),"color" => $this->m->error()
            ], 404);
          }
    }
}
