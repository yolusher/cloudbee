<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\model\m_treatment;
use App\response\message;

class c_treatment extends Controller
{
    private $m;

    public function __construct()
    {
        $this->m = new message;
    }


    public function index($company)
    {
        $treatment = m_treatment::where('company', $company)->get()->toJson(JSON_PRETTY_PRINT);
        return response($treatment, 200);
    }
    public function getbyillness($company,$code)
    {
        $treatment = m_treatment::where('company', $company)->where('illness', $code)->get()->toJson(JSON_PRETTY_PRINT);
        return response($treatment, 200);
    }
    public function create()
    {

    }

    public function store(Request $request,$company)
    {
        
        if(m_treatment::where('code', $request->code)->exists()){
            return response()->json(["message" => $this->m->duplicate(),"color" => $this->m->error()], 201);
        }
        $treatment = new m_treatment;
        $treatment->code = $request->code;
        $treatment->description = $request->description;
        $treatment->company = $request->company;
        $treatment->illness = $request->illness;
        $treatment->unit_measure = $request->unit_measure;
        $treatment->state = $request->state;
        $treatment->save();
        return response()->json([
          "message" =>  $this->m->create(),"color" => $this->m->success()
        ], 201);
    }

    public function show($company,$id)
    {
        if (m_treatment::where('id', $id)->exists()) {
            $treatment = m_treatment::where('id', $id)->get()->toJson(JSON_PRETTY_PRINT);
            return response($treatment, 200);
        } else {
            return response()->json([
              "message" => $this->m->NotFound(),"color" => $this->m->error()
            ], 404);
        }
        return m_treatment::where('id',$id)->get();
    }
    public function combobox($company)
    {
        $treatment = m_treatment::where('company', $company)->where('state', true)->select('code','name')->get()->toJson(JSON_PRETTY_PRINT);
        return response($treatment, 200);
       
    }
    public function edit($id,$company)
    {
        //
    }

    public function update(Request $request, $company,$id)
    {
        if (m_treatment::where('id', $id)->exists()) {
            $treatment = m_treatment::find($id);
            $treatment->code = is_null($request->code) ? $treatment->code : $request->code;
            $treatment->description = is_null($request->description) ? $treatment->description : $request->description;
            $treatment->state = is_null($request->state) ? $treatment->state : $request->state;
            $treatment->company = is_null($request->company) ? $treatment->company : $request->company;
            $treatment->illness = is_null($request->illness) ? $treatment->illness : $request->illness;
            $treatment->unit_measure = is_null($request->unit_measure) ? $treatment->unit_measure : $request->unit_measure;
            $treatment->save();
            return response()->json([
                "message" => $this->m->update(),"color" => $this->m->success()
            ], 200);
        } else {
            return response()->json([
                "message" => $this->m->NotFound(),"color" => $this->m->error()
            ], 404);
            
        }
    }

    public function destroy($company,$id)
    {
        if(m_treatment::where('id', $id)->exists()) {
            $treatment = m_treatment::find($id);
            $treatment->delete();

            return response()->json([
              "message" => $this->m->delete(),"color" => $this->m->success()
            ], 202);
          } else {
            return response()->json([
              "message" => $this->m->NotFound(),"color" => $this->m->error()
            ], 404);
          }
    }
}
