<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\model\m_hives_type;
use App\response\message;

class c_hives_type extends Controller
{
    private $m;

    public function __construct()
    {
        $this->m = new message;
    }


    public function index($company)
    {
        $hives_type = m_hives_type::where('company', $company)->get()->toJson(JSON_PRETTY_PRINT);
        return response($hives_type, 200);
    }

    
    public function create()
    {

    }

    public function store(Request $request,$company)
    {
        
        if(m_hives_type::where('code', $request->code)->exists()){
            return response()->json(["message" => $this->m->duplicate(),"color" => $this->m->error()], 201);
        }
        $hives_type = new m_hives_type;
        $hives_type->code = $request->code;
        $hives_type->name = $request->name;
        $hives_type->company = $request->company;
        $hives_type->state = $request->state;
        $hives_type->save();
        return response()->json([
          "message" =>  $this->m->create(),"color" => $this->m->success()
        ], 201);
    }

    public function show($company,$id)
    {
        if (m_hives_type::where('id', $id)->exists()) {
            $hives_type = m_hives_type::where('id', $id)->get()->toJson(JSON_PRETTY_PRINT);
            return response($hives_type, 200);
        } else {
            return response()->json([
              "message" => $this->m->NotFound(),"color" => $this->m->error()
            ], 404);
        }
        return m_hives_type::where('id',$id)->get();
    }
    public function combobox($company)
    {
        $hives_type = m_hives_type::where('company', $company)->where('state', true)->select('code','name')->get()->toJson(JSON_PRETTY_PRINT);
        return response($hives_type, 200);
       
    }
    public function edit($id,$company)
    {
        //
    }

    public function update(Request $request, $company,$id)
    {
        if (m_hives_type::where('id', $id)->exists()) {
            $hives_type = m_hives_type::find($id);
            $hives_type->code = is_null($request->code) ? $hives_type->code : $request->code;
            $hives_type->name = is_null($request->name) ? $hives_type->name : $request->name;
            $hives_type->state = is_null($request->state) ? $hives_type->state : $request->state;
            $hives_type->company = is_null($request->company) ? $hives_type->company : $request->company;
            $hives_type->save();
            return response()->json([
                "message" => $this->m->update(),"color" => $this->m->success()
            ], 200);
        } else {
            return response()->json([
                "message" => $this->m->NotFound(),"color" => $this->m->error()
            ], 404);
            
        }
    }

    public function destroy($company,$id)
    {
        if(m_hives_type::where('id', $id)->exists()) {
            $hives_type = m_hives_type::find($id);
            $hives_type->delete();

            return response()->json([
              "message" => $this->m->delete(),"color" => $this->m->success()
            ], 202);
          } else {
            return response()->json([
              "message" => $this->m->NotFound(),"color" => $this->m->error()
            ], 404);
          }
    }
}
