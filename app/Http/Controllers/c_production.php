<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\model\m_production;
use App\response\message;

class c_production extends Controller
{
    private $m;

    public function __construct()
    {
        $this->m = new message;
    }


    public function index($company)
    {
        $production = m_production::where('company', $company)->get()->toJson(JSON_PRETTY_PRINT);
        return response($production, 200);
    }

    
    public function create()
    {

    }

    public function store(Request $request,$company)
    {
        
        if(m_production::where('code', $request->code)->exists()){
            return response()->json(["message" => $this->m->duplicate(),"color" => $this->m->error()], 201);
        }
        $production = new m_production;
        $production->code = $request->code;
        $production->name = $request->name;
        $production->unit_measure = $request->unit_measure;
        $production->company = $request->company;
        $production->state = $request->state;
        $production->save();
        return response()->json([
          "message" =>  $this->m->create(),"color" => $this->m->success()
        ], 201);
    }

    public function show($company,$id)
    {
        if (m_production::where('id', $id)->exists()) {
            $production = m_production::where('id', $id)->get()->toJson(JSON_PRETTY_PRINT);
            return response($production, 200);
        } else {
            return response()->json([
              "message" => $this->m->NotFound(),"color" => $this->m->error()
            ], 404);
        }
        return m_production::where('id',$id)->get();
    }
    public function combobox($company)
    {
        $production = m_production::where('company', $company)->where('state', true)->select('code','name')->get()->toJson(JSON_PRETTY_PRINT);
        return response($production, 200);
       
    }
    public function edit($id,$company)
    {
        //
    }

    public function update(Request $request, $company,$id)
    {
        if (m_production::where('id', $id)->exists()) {
            $production = m_production::find($id);
            $production->code = is_null($request->code) ? $production->code : $request->code;
            $production->name = is_null($request->name) ? $production->name : $request->name;
            $production->unit_measure = is_null($request->unit_measure) ? $production->unit_measure : $request->unit_measure;
            $production->state = is_null($request->state) ? $production->state : $request->state;
            $production->company = is_null($request->company) ? $production->company : $request->company;
            $production->save();
            return response()->json([
                "message" => $this->m->update(),"color" => $this->m->success()
            ], 200);
        } else {
            return response()->json([
                "message" => $this->m->NotFound(),"color" => $this->m->error()
            ], 404);
            
        }
    }

    public function destroy($company,$id)
    {
        if(m_production::where('id', $id)->exists()) {
            $production = m_production::find($id);
            $production->delete();

            return response()->json([
              "message" => $this->m->delete(),"color" => $this->m->success()
            ], 202);
          } else {
            return response()->json([
              "message" => $this->m->NotFound(),"color" => $this->m->error()
            ], 404);
          }
    }
}
