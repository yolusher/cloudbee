<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\model\m_equipment;
use App\model\m_storage;
use App\model\m_global;
use App\model\m_meekness;
use App\model\m_problem;
use App\model\m_elements;
use App\model\m_production;
use App\model\m_mother;
use App\model\m_illness;
use App\model\m_treatment;
use App\model\m_task;


class c_getform extends Controller
{

    public function getform($company)
    {
        $json = array();
        
        $equipment = m_equipment::where('company', $company)->where('state', true)->select('name','_number','honey','breeding')->get();
        $equipment_json = array('name'=>'equipment','title'=>'EQUIPO','type'=>'table','required'=>true,'display'=>'selected','selected'=>true,'data'=>$equipment);
        
        array_push($json,$equipment_json);
        
        $storage = m_storage::where('company', $company)->where('state', true)->select('name as key','name as label')->get();
        $storage_json = array('name'=>'storage','title'=>'CAPACIDAD DE ALMACENAMIENTO','type'=>'select','required'=>true,'display'=>'selected','selected'=>true,'options'=>$storage);

        $storage = m_storage::where('company', $company)->where('state', true)->select('name as key','name as label')->get();
        $storage_json = array('name'=>'storage','title'=>'CAPACIDAD PRODUCTIVA','type'=>'select','required'=>true,'display'=>'selected','selected'=>true,'options'=>$storage);
        
        array_push($json,$storage_json);

        $global = m_global::where('company', $company)->where('state', true)->where('_type', 'REVISIÓN')->select('name','unit')->get();
        $global_json = array('name'=>'hygienic','title'=>$global[0]["name"],'um'=>$global[0]["unit"],'type'=>'text','required'=>true,'display'=>'selected','selected'=>true);
        
        array_push($json,$global_json);

        $storage = m_meekness::where('company', $company)->where('state', true)->select('name as key','name as label')->get();
        $meekness_json = array('name'=>'meekness','title'=>"MANSEDUMBRE",'type'=>'select','required'=>true,'display'=>'selected','selected'=>true,'options'=>$storage);
        
        array_push($json,$meekness_json);

        $storage = m_problem::where('company', $company)->where('state', true)->select('code','description')->get();
        $task = array();
        foreach ($storage as $value) {
            $temp = m_task::where('company', $company)->where('state', true)->where('problem', $value["code"])->select('description as name')->get();
            $store = array('name'=>$value["description"],"task"=>$temp);
            array_push($task, $store);
        }
        $problem_json = array('name'=>'problems','title'=>"PROBLEMAS",'type'=>'multipleselect','data'=>$task);
        array_push($json,$problem_json);

        $storage = m_elements::where('company', $company)->where('state', true)->select('name as label','unit_measure as um')->get();
        $elements_json = array('name'=>'elements','title'=>"ALIMENTOS, PRODUCTOS Y EQUIPOS",'type'=>'multipleinput','required'=>true,'display'=>'selected','selected'=>true,'data'=>$storage);
        
        array_push($json,$elements_json);

        $storage = m_production::where('company', $company)->where('state', true)->select('name as label','unit_measure as um')->get();
        $production_json = array('name'=>'production','title'=>"PRODUCCIÓN",'type'=>'multipleinput','required'=>true,'display'=>'selected','selected'=>true,'data'=>$storage);
        
        array_push($json,$production_json);
        
        //quen
        $storage = m_mother::where('company', $company)->where('state', true)->select('code as key','code as label')->get();
        $queen_json = array('name'=>'queen','title'=>"REYNA",'type'=>'select','required'=>true,'display'=>'selected','selected'=>true,'options'=>$storage);
        array_push($json,$queen_json);
        //storageproductive

        
        //illness
        $storage = m_illness::where('company', $company)->where('state', true)->select('code','description')->get();
        $treatment = array();
        foreach ($storage as $value) {
            $temp = m_treatment::where('company', $company)->where('state', true)->where('illness', $value["code"])->select('description as name','unit_measure as um')->get();
            $store = array('name'=>$value["description"],"treatment"=>$temp);
            array_push($treatment, $store);
        }
        $ill_json = array('name'=>'illness','title'=>"ENFERMEDADES",'type'=>'multipleselect','data'=>$treatment);
        array_push($json,$ill_json);




        return response(json_encode($json), 200);

    }


}
