<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\model\m_unit_measure;
use App\response\message;

class c_unit_measure extends Controller
{
    private $m;

    public function __construct()
    {
        $this->m = new message;
    }


    public function index($company)
    {
        $unit_measure = m_unit_measure::where('company', $company)->get()->toJson(JSON_PRETTY_PRINT);
        return response($unit_measure, 200);
    }

    
    public function create()
    {

    }

    public function store(Request $request,$company)
    {
        
        if(m_unit_measure::where('code', $request->code)->exists()){
            return response()->json(["message" => $this->m->duplicate(),"color" => $this->m->error()], 201);
        }
        $unit_measure = new m_unit_measure;
        $unit_measure->code = $request->code;
        $unit_measure->name = $request->name;
        $unit_measure->company = $request->company;
        $unit_measure->sym = $request->sym;
        $unit_measure->state = $request->state;
        $unit_measure->save();
        return response()->json([
          "message" =>  $this->m->create(),"color" => $this->m->success()
        ], 201);
    }

    public function show($company,$id)
    {
        if (m_unit_measure::where('id', $id)->exists()) {
            $unit_measure = m_unit_measure::where('id', $id)->get()->toJson(JSON_PRETTY_PRINT);
            return response($unit_measure, 200);
        } else {
            return response()->json([
              "message" => $this->m->NotFound(),"color" => $this->m->error()
            ], 404);
        }
        return m_unit_measure::where('id',$id)->get();
    }
    public function combobox($company)
    {
        $unit_measure = m_unit_measure::where('company', $company)->where('state', true)->select('sym','name')->get()->toJson(JSON_PRETTY_PRINT);
        return response($unit_measure, 200);
       
    }
    public function edit($id,$company)
    {
        //
    }

    public function update(Request $request, $company,$id)
    {
        if (m_unit_measure::where('id', $id)->exists()) {
            $unit_measure = m_unit_measure::find($id);
            $unit_measure->code = is_null($request->code) ? $unit_measure->code : $request->code;
            $unit_measure->name = is_null($request->name) ? $unit_measure->name : $request->name;
            $unit_measure->state = is_null($request->state) ? $unit_measure->state : $request->state;
            $unit_measure->company = is_null($request->company) ? $unit_measure->company : $request->company;
            $unit_measure->sym = is_null($request->sym) ? $unit_measure->sym : $request->sym;
            $unit_measure->save();
            return response()->json([
                "message" => $this->m->update(),"color" => $this->m->success()
            ], 200);
        } else {
            return response()->json([
                "message" => $this->m->NotFound(),"color" => $this->m->error()
            ], 404);
            
        }
    }

    public function destroy($company,$id)
    {
        if(m_unit_measure::where('id', $id)->exists()) {
            $unit_measure = m_unit_measure::find($id);
            $unit_measure->delete();

            return response()->json([
              "message" => $this->m->delete(),"color" => $this->m->success()
            ], 202);
          } else {
            return response()->json([
              "message" => $this->m->NotFound(),"color" => $this->m->error()
            ], 404);
          }
    }
}
