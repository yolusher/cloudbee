<?php

namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\model\m_children;
use App\response\message;
use Illuminate\Support\Facades\DB;

class c_children extends Controller
{
    private $m;

    public function __construct()
    {
        $this->m = new message;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($company)
    {
        //
        /*$bee_father = DB::table('mst_bees_children')
            ->join('mst_bees_mother','mst_bees_mother.code','=','mst_queen_race.race')
            ->select('mst_bees_mother.*', 'mst_hives_origin.name AS name_origin', 'mst_queen_race.name AS name_race')
            ->where('mst_bees_father.company', $company)
            ->get()
            ->toJson(JSON_PRETTY_PRINT);
        return response($bee_father, 200);
*/
        $bee_children = m_children::where('company', $company)->get()->toJson(JSON_PRETTY_PRINT);
        return response($bee_children, 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request,$company)
    {
        //
        if(m_children::where('code', $request->code)->exists()){
            return response()->json(["message" => $this->m->duplicate(),"color" => $this->m->error()], 201);
        }
        $bee_children = new m_children;
        $bee_children->code = $request->mother.'-'.$request->father;
        $bee_children->mother = $request->mother;
        $bee_children->father = $request->father;
        $bee_children->type_bee = $request->type_bee;
        $bee_children->insertion_date = $request->insertion_date;
        $bee_children->status = $request->status;
        $bee_children->company = $request->company;
        $bee_children->state = $request->state;
        $bee_children->save();
        $insertedId = $bee_children->id;
        $bee_children = m_children::find($insertedId);
        $bee_children->code = $request->mother.'-'.$request->father.'-'.$insertedId;
        $bee_children->save();
        return response()->json([
          "message" =>  $this->m->create(),"color" => $this->m->success()
        ], 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($company,$id)
    {
        //
        if (m_children::where('id', $id)->exists()) {
            $bee_children = m_children::where('id', $id)->get()->toJson(JSON_PRETTY_PRINT);
            return response($bee_children, 200);
        } else {
            return response()->json([
              "message" => $this->m->NotFound(),"color" => $this->m->error()
            ], 404);
        }
        return m_children::where('id',$id)->get();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function combobox($company)
    {
        $bee_children = m_children::where('company', $company)->where('state', true)->select('code')->get()->toJson(JSON_PRETTY_PRINT);
        return response($bee_children, 200);
       
    }
    public function comboboxnoassig($company)
    {
        $bee_children = m_children::where('company', $company)->where('state', true)->where('status', '2')->select('code')->get()->toJson(JSON_PRETTY_PRINT);
        return response($bee_children, 200);
       
    }
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$company, $id)
    {
        //
        if (m_children::where('id', $id)->exists()) {
            $bee_children = m_children::find($id);
            $bee_children->code = is_null($request->code) ? $bee_children->code : $request->code;
            $bee_children->mother = is_null($request->mother) ? $bee_children->mother : $request->mother;
            $bee_children->father = is_null($request->father) ? $bee_children->father : $request->father;
            $bee_children->type_bee = is_null($request->type_bee) ? $bee_children->type_bee : $request->type_bee;
            $bee_children->insertion_date = is_null($request->insertion_date) ? $bee_children->insertion_date : $request->insertion_date;
            $bee_children->status = is_null($request->status) ? $bee_children->status : $request->status;
            $bee_children->company = is_null($request->company) ? $bee_children->company : $request->company;
            $bee_children->save();
            return response()->json([
                "message" => $this->m->update(),"color" => $this->m->success()
            ], 200);
        } else {
            return response()->json([
                "message" => $this->m->NotFound(),"color" => $this->m->error()
            ], 404);
            
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        if(m_children::where('id', $id)->exists()) {
            $bee_children = m_children::find($id);
            $bee_children->delete();

            return response()->json([
                "message" => $this->m->delete(),"color" => $this->m->success()
            ], 202);
        } else {
            return response()->json([
                "message" => $this->m->NotFound(),"color" => $this->m->error()
            ], 404);
        }
    }
}
