<?php

namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use App\response\message;

class c_user extends Controller
{
    private $m;

    public function __construct()
    {
        $this->m = new message;
    }


    public function index($company)
    {
        $user = User::where('company', $company)->get()->toJson(JSON_PRETTY_PRINT);
        return response($user, 200);
    }

    

    public function store(Request $request,$company)
    {
        
        if(User::where('id', $request->id)->exists()){
            return response()->json(["message" => $this->m->duplicate(),"color" => $this->m->error()], 201);
        }
        $user = new User;
        $user->name = $request->name;
        $user->username = $request->username;
        $user->email = $request->email;
        $user->password = bcrypt($request->password);
        $user->company = $company;
        $user->active = $request->active;
        $user->role = "ADMINISTRADOR";
        $user->save();
        return response()->json([
          "message" =>  $this->m->create(),"color" => $this->m->success()
        ], 201);
    }

    public function show($company,$id)
    {
        if (User::where('id', $id)->exists()) {
            $user = User::where('id', $id)->get()->toJson(JSON_PRETTY_PRINT);
            return response($user, 200);
        } else {
            return response()->json([
              "message" => $this->m->NotFound(),"color" => $this->m->error()
            ], 404);
        }
        return User::where('id',$id)->get();
    }
    public function combobox($company)
    {
        $user = User::where('company', $company)->where('state', true)->select('id','name')->get()->toJson(JSON_PRETTY_PRINT);
        return response($user, 200);
       
    }
    public function edit($id,$company)
    {
        //
    }

    public function update(Request $request, $company,$id)
    {
        if (User::where('id', $id)->exists()) {
            $user = User::find($id);
            $user->name = is_null($request->name) ? $user->name : $request->name;
            $user->username = is_null($request->username) ? $user->username : $request->username;
            $user->email = is_null($request->email) ? $user->email : $request->email;
            $user->password = is_null($request->password) ? $user->password : bcrypt($request->password);
            $user->active = is_null($request->active) ? $user->active : $request->active;
            $user->save();
            return response()->json([
                "message" => $this->m->update(),"color" => $this->m->success()
            ], 200);
        } else {
            return response()->json([
                "message" => $this->m->NotFound(),"color" => $this->m->error()
            ], 404);
            
        }
    }

    public function destroy($company,$id)
    {
        if(User::where('id', $id)->exists()) {
            $user = User::find($id);
            $user->delete();

            return response()->json([
              "message" => $this->m->delete(),"color" => $this->m->success()
            ], 202);
          } else {
            return response()->json([
              "message" => $this->m->NotFound(),"color" => $this->m->error()
            ], 404);
          }
    }
}
