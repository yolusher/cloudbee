<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\model\m_supplier;
use App\response\message;
use Illuminate\Support\Facades\DB;

class c_supplier extends Controller
{
    private $m;

    public function __construct()
    {
        $this->m = new message;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($company)
    {
        //
        //$supplier = m_supplier::where('company', $company)->get()->toJson(JSON_PRETTY_PRINT);
        //return response($supplier, 200);

        $supplier = DB::table('mst_bees_supplier')
            ->join('mst_hives_origin', 'mst_bees_supplier.origin', '=', 'mst_hives_origin.code')
            ->select('mst_bees_supplier.*', 'mst_hives_origin.name AS name_origin')
            ->where('mst_bees_supplier.company', $company)
            ->get()
            ->toJson(JSON_PRETTY_PRINT);
        return response($supplier, 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request,$company)
    {
        //
        if(m_supplier::where('code', $request->code)->exists()){
            return response()->json(["message" => $this->m->duplicate(),"color" => $this->m->error()], 201);
        }
        $supplier = new m_supplier;
        $supplier->code = $request->code;
        $supplier->name = $request->name;
        $supplier->origin = $request->origin;
        $supplier->company = $request->company;
        $supplier->state = $request->state;
        $supplier->save();
        return response()->json([
          "message" =>  $this->m->create(),"color" => $this->m->success()
        ], 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($company,$id)
    {
        //
        if (m_supplier::where('id', $id)->exists()) {
            $supplier = m_supplier::where('id', $id)->get()->toJson(JSON_PRETTY_PRINT);
            return response($supplier, 200);
        } else {
            return response()->json([
              "message" => $this->m->NotFound(),"color" => $this->m->error()
            ], 404);
        }
        return m_supplier::where('id',$id)->get();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function combobox($company)
    {
        $supplier = m_supplier::where('company', $company)->where('state', true)->select('code','name')->get()->toJson(JSON_PRETTY_PRINT);
        return response($supplier, 200);
       
    }

    public function comboboxcode($code,$company)
    {
        $supplier = m_supplier::where('company', $code)->where('origin', $company)->select('code','name')->get()->toJson(JSON_PRETTY_PRINT);
        return response($supplier, 200);
        //return $code.'-'.$company;
    }

    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$company,$id)
    {
        //
        if (m_supplier::where('id', $id)->exists()) {
            $supplier = m_supplier::find($id);
            $supplier->code = is_null($request->code) ? $supplier->code : $request->code;
            $supplier->name = is_null($request->name) ? $supplier->name : $request->name;
            $supplier->state = is_null($request->state) ? $supplier->state : $request->state;
            $supplier->origin = is_null($request->origin) ? $supplier->origin : $request->origin;
            $supplier->company = is_null($request->company) ? $supplier->company : $request->company;
            $supplier->save();
            return response()->json([
                "message" => $this->m->update(),"color" => $this->m->success()
            ], 200);
        } else {
            return response()->json([
                "message" => $this->m->NotFound(),"color" => $this->m->error()
            ], 404);
            
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        if(m_supplier::where('id', $id)->exists()) {
            $supplier = m_supplier::find($id);
            $supplier->delete();

            return response()->json([
              "message" => $this->m->delete(),"color" => $this->m->success()
            ], 202);
          } else {
            return response()->json([
              "message" => $this->m->NotFound(),"color" => $this->m->error()
            ], 404);
          }
    }
}
