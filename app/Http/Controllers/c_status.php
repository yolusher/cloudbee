<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\model\m_status;
use App\response\message;

class c_status extends Controller
{
    private $m;

    public function __construct()
    {
        $this->m = new message;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($company)
    {
        //
        $status = m_status::where('company', $company)->get()->toJson(JSON_PRETTY_PRINT);
        return response($status, 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request,$company)
    {
        //
        if(m_status::where('code', $request->code)->exists()){
            return response()->json(["message" => $this->m->duplicate(),"color" => $this->m->error()], 201);
        }
        $status = new m_status;
        $status->code = $request->code;
        $status->name = $request->name;
        $status->company = $request->company;
        $status->state = $request->state;
        $status->save();
        return response()->json([
          "message" =>  $this->m->create(),"color" => $this->m->success()
        ], 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($company,$id)
    {
        //
        if (m_status::where('id', $id)->exists()) {
            $status = m_status::where('id', $id)->where('company', $id)->get()->toJson(JSON_PRETTY_PRINT);
            return response($status, 200);
        } else {
            return response()->json([
              "message" => $this->m->NotFound(),"color" => $this->m->error()
            ], 404);
        }
        return m_status::where('id',$id)->where('company',$company)->get();
    }
    
    
    public function combobox($company)
    {
        $status = m_status::where('company', $company)->where('state', true)->select('code','name')->get()->toJson(JSON_PRETTY_PRINT);
        return response($status, 200);
       
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$company,$id)
    {
        //
        if (m_status::where('id', $id)->exists()) {
            $status = m_status::find($id);
            $status->code = is_null($request->code) ? $status->code : $request->code;
            $status->name = is_null($request->name) ? $status->name : $request->name;
            $status->state = is_null($request->state) ? $status->state : $request->state;
            $status->company = is_null($request->company) ? $status->company : $request->company;
            $status->save();
            return response()->json([
                "message" => $this->m->update(),"color" => $this->m->success()
            ], 200);
        } else {
            return response()->json([
                "message" => $this->m->NotFound(),"color" => $this->m->error()
            ], 404);
            
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($company,$id)
    {
        //
        if(m_status::where('id', $id)->exists()) {
            $status = m_status::find($id);
            $status->delete();

            return response()->json([
              "message" => $this->m->delete(),"color" => $this->m->success()
            ], 202);
          } else {
            return response()->json([
              "message" => $this->m->NotFound(),"color" => $this->m->error()
            ], 404);
          }
    }
}
