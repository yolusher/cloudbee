<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\model\m_company;
use App\response\message;

class c_company extends Controller
{
    private $m;

    public function __construct()
    {
        $this->m = new message;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
         $_company = DB::table('conf_company')
            ->join('conf_company_plans','conf_company_plans.code','=','conf_company.plan')
            ->join('conf_company_payment','conf_company_payment.company','=','conf_company.code')
            ->select('conf_company.*', 'conf_company_plans.name AS name_plan','conf_company_plans.max_hives AS max_hives', 'conf_company_payment.name AS name_pay')
            ->get()
            ->toJson(JSON_PRETTY_PRINT);
        //return response($bee_mother, 200);
        //$_company = m_company::get()->toJson(JSON_PRETTY_PRINT);
        return response($_company, 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        if (m_company::where('id', $id)->exists()) {
            $_company = m_company::where('id', $id)->get()->toJson(JSON_PRETTY_PRINT);
            return response($_company, 200);
        } else {
            return response()->json([
              "message" => $this->m->NotFound(),"color" => $this->m->error()
            ], 404);
        }
        return m_company::where('id',$id)->get();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        if (m_company::where('id', $id)->exists()) {
            $_company = m_company::find($id);
            $_company->code = is_null($request->code) ? $_company->code : $request->code;
            $_company->name = is_null($request->name) ? $_company->name : $request->name;
            $_company->plan = is_null($request->plan) ? $_company->plan : $request->plan;
            $_company->state = is_null($request->state) ? $_company->state : $request->state;
            $_company->save();
            return response()->json([
                "message" => $this->m->update(),"color" => $this->m->success()
            ], 200);
        } else {
            return response()->json([
                "message" => $this->m->NotFound(),"color" => $this->m->error()
            ], 404);
        
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
