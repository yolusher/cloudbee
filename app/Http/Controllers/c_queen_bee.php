<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\model\m_queen_race;
use App\response\message;

class c_queen_bee extends Controller
{
    private $m;

    public function __construct()
    {
        $this->m = new message;
    }


    public function index($company)
    {
        $queen_bee = m_queen_race::where('company', $company)->get()->toJson(JSON_PRETTY_PRINT);
        return response($queen_bee, 200);
    }

    
    public function create()
    {

    }

    public function store(Request $request,$company)
    {
        
        if(m_queen_race::where('code', $request->code)->exists()){
            return response()->json(["message" => $this->m->duplicate(),"color" => $this->m->error()], 201);
        }
        $queen_bee = new m_queen_race;
        $queen_bee->code = $request->code;
        $queen_bee->name = $request->name;
        $queen_bee->company = $request->company;
        $queen_bee->state = $request->state;
        $queen_bee->save();
        return response()->json([
          "message" =>  $this->m->create(),"color" => $this->m->success()
        ], 201);
    }

    public function show($company,$id)
    {
        if (m_queen_race::where('id', $id)->exists()) {
            $queen_bee = m_queen_race::where('id', $id)->get()->toJson(JSON_PRETTY_PRINT);
            return response($queen_bee, 200);
        } else {
            return response()->json([
              "message" => $this->m->NotFound(),"color" => $this->m->error()
            ], 404);
        }
        return m_queen_race::where('id',$id)->get();
    }
    public function combobox($company)
    {
        $queen_bee = m_queen_race::where('company', $company)->where('state', true)->select('code','name')->get()->toJson(JSON_PRETTY_PRINT);
        return response($queen_bee, 200);
       
    }
    public function edit($id,$company)
    {
        //
    }

    public function update(Request $request, $company,$id)
    {
        if (m_queen_race::where('id', $id)->exists()) {
            $queen_bee = m_queen_race::find($id);
            $queen_bee->code = is_null($request->code) ? $queen_bee->code : $request->code;
            $queen_bee->name = is_null($request->name) ? $queen_bee->name : $request->name;
            $queen_bee->state = is_null($request->state) ? $queen_bee->state : $request->state;
            $queen_bee->company = is_null($request->company) ? $queen_bee->company : $request->company;
            $queen_bee->save();
            return response()->json([
                "message" => $this->m->update(),"color" => $this->m->success()
            ], 200);
        } else {
            return response()->json([
                "message" => $this->m->NotFound(),"color" => $this->m->error()
            ], 404);
            
        }
    }

    public function destroy($company,$id)
    {
        if(m_queen_race::where('id', $id)->exists()) {
            $queen_bee = m_queen_race::find($id);
            $queen_bee->delete();

            return response()->json([
              "message" => $this->m->delete(),"color" => $this->m->success()
            ], 202);
          } else {
            return response()->json([
              "message" => $this->m->NotFound(),"color" => $this->m->error()
            ], 404);
          }
    }
}
