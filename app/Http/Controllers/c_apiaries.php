<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\model\m_apiaries;
use App\response\message;

class c_apiaries extends Controller
{
    private $m;

    public function __construct()
    {
        $this->m = new message;
    }
public function apiaryservice($company)
    {
        $apiaries = m_apiaries::where('company', $company)->where('state', true)->select('id','code','name','address','latitude','longitude')->get()->toJson(JSON_PRETTY_PRINT);
        return response($apiaries, 200);
       
    }

    public function index($company)
    {
        $apiaries = m_apiaries::where('company', $company)->get()->toJson(JSON_PRETTY_PRINT);
        return response($apiaries, 200);
    }

    
    public function create()
    {

    }

    public function store(Request $request,$company)
    {
        
        if(m_apiaries::where('code', $request->code)->exists()){
            return response()->json(["message" => $this->m->duplicate(),"color" => $this->m->error()], 201);
        }
        $apiaries = new m_apiaries;
        $apiaries->code = $request->code;
        $apiaries->name = $request->name;
        $apiaries->company = $request->company;
        $apiaries->address = $request->address;
        $apiaries->description = $request->description;
        $apiaries->latitude = $request->latitude;
        $apiaries->longitude = $request->longitude;
        $apiaries->state = $request->state;
        $apiaries->save();
        return response()->json([
          "message" =>  $this->m->create(),"color" => $this->m->success()
        ], 201);
    }

    public function show($company,$id)
    {
        if (m_apiaries::where('id', $id)->exists()) {
            $apiaries = m_apiaries::where('id', $id)->get()->toJson(JSON_PRETTY_PRINT);
            return response($apiaries, 200);
        } else {
            return response()->json([
              "message" => $this->m->NotFound(),"color" => $this->m->error()
            ], 404);
        }
        return m_apiaries::where('id',$id)->get();
    }
    public function combobox($company)
    {
        $apiaries = m_apiaries::where('company', $company)->where('state', true)->select('code','name')->get()->toJson(JSON_PRETTY_PRINT);
        return response($apiaries, 200);
       
    }

    /*public function apiaryservice($company)
    {
        $apiaries = m_apiaries::where('company', $company)->where('state', true)->select('id','code','name','address','latitude','longitude')->get()->toJson(JSON_PRETTY_PRINT);
        return response($apiaries, 200);
       
    }*/
    public function edit($id,$company)
    {
        //
    }

    public function update(Request $request, $company,$id)
    {
        if (m_apiaries::where('id', $id)->exists()) {
            $apiaries = m_apiaries::find($id);
            $apiaries->code = is_null($request->code) ? $apiaries->code : $request->code;
            $apiaries->name = is_null($request->name) ? $apiaries->name : $request->name;
            $apiaries->state = is_null($request->state) ? $apiaries->state : $request->state;
            $apiaries->company = is_null($request->company) ? $apiaries->company : $request->company;
            $apiaries->address = is_null($request->address) ? $apiaries->address : $request->address;
            $apiaries->description = is_null($request->description) ? $apiaries->description : $request->description;
            $apiaries->latitude = is_null($request->latitude) ? $apiaries->latitude : $request->latitude;
            $apiaries->longitude = is_null($request->longitude) ? $apiaries->longitude : $request->longitude;
            $apiaries->save();
            return response()->json([
                "message" => $this->m->update(),"color" => $this->m->success()
            ], 200);
        } else {
            return response()->json([
                "message" => $this->m->NotFound(),"color" => $this->m->error()
            ], 404);
            
        }
    }

    public function destroy($company,$id)
    {
        if(m_apiaries::where('id', $id)->exists()) {
            $apiaries = m_apiaries::find($id);
            $apiaries->delete();

            return response()->json([
              "message" => $this->m->delete(),"color" => $this->m->success()
            ], 202);
          } else {
            return response()->json([
              "message" => $this->m->NotFound(),"color" => $this->m->error()
            ], 404);
          }
    }
}
