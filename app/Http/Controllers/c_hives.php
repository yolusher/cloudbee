<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\model\m_hives;
use App\model\m_children;
use App\model\m_equipment;
use App\response\message;
use Illuminate\Support\Facades\DB;

class c_hives extends Controller
{
    private $m;

    public function __construct()
    {
        $this->m = new message;
    }

    public function hiveservice($company)
    {
        $hives_app = DB::table('mst_hives')
            ->join('mst_hives_origin', 'mst_hives.origin_hives', '=', 'mst_hives_origin.code')
            ->join('mst_hives_type', 'mst_hives.type_hives', '=', 'mst_hives_type.code')
            ->join('mst_queen_race', 'mst_hives.queen_race', '=', 'mst_queen_race.code')
            ->join('mst_apiaries', 'mst_hives.apiaries', '=', 'mst_apiaries.code')
            ->select('mst_hives.id','mst_hives.code','mst_hives.type_hives AS type','mst_hives.date_installed AS date_installation', 'mst_hives_origin.name AS origin', 'mst_hives.apiaries AS codeapiary','mst_queen_race.name AS queenrace','mst_hives_type.name AS name_type_hives','mst_apiaries.name AS nameapiary')
            ->where('mst_hives.company', $company)
            ->get()
            ->toJson(JSON_PRETTY_PRINT);
        return response($hives_app, 200);
       
    }

    public function index($company)
    {
         $queen_bee = DB::table('mst_hives')
            ->join('mst_hives_origin', 'mst_hives.origin_hives', '=', 'mst_hives_origin.code')
            ->join('mst_hives_type', 'mst_hives.type_hives', '=', 'mst_hives_type.code')
            ->join('mst_queen_race', 'mst_hives.queen_race', '=', 'mst_queen_race.code')
            ->join('mst_apiaries', 'mst_hives.apiaries', '=', 'mst_apiaries.code')
            ->select('mst_hives.*', 'mst_hives_origin.name AS name_origin_hives','mst_queen_race.name AS name_queen_race','mst_hives_type.name AS name_type_hives','mst_apiaries.name AS name_apiaries')
            ->where('mst_hives.company', $company)
            ->get()
            ->toJson(JSON_PRETTY_PRINT);
        return response($queen_bee, 200);

    }

    
    public function create()
    {

    }

    public function store(Request $request,$company)
    {
        
        /*if(m_hives::where('code', $request->code)->exists()){
            return response()->json(["message" => $this->m->duplicate(),"color" => $this->m->error()], 201);
        }*/
        
        for ($i = 1; $i <=  $request->quantity; $i++) {
            $queen_bee = new m_hives;
            $queen_bee->code = $this->getcode($company);
            $queen_bee->company = $request->company;
            $queen_bee->state = $request->state;
            $queen_bee->type_hives = $request->type_hives;
            $queen_bee->origin_hives = $request->origin_hives;
            $queen_bee->apiaries = $request->apiaries;
            $queen_bee->queen_race = $request->queen_race;
            $queen_bee->queen_id = $request->queen_id;
            $queen_bee->date_installed = $request->date_installed;
            $queen_bee->rice = 6;
            $queen_bee->save();
        }
        if($request->queen_id!='000'){
            $id = explode("-", $request->queen_id);
            $bee_children = m_children::find($id[2]);
            $bee_children->status = '001';
            $bee_children->save();
        }
        return response()->json([
          "message" =>  $this->m->create(),"color" => $this->m->success()
        ], 201);
    } 
    public function show($company,$id)
    {
        if (m_hives::where('id', $id)->exists()) {
            $queen_bee = m_hives::where('id', $id)->get()->toJson(JSON_PRETTY_PRINT);
            return response($queen_bee, 200);
        } else {
            return response()->json([
              "message" => $this->m->NotFound(),"color" => $this->m->error()
            ], 404);
        }
        return m_hives::where('id',$id)->get();
    }
    
    public function combobox($company)
    {
        $queen_bee = m_hives::where('company', $company)->where('state', true)->select('code')->get()->toJson(JSON_PRETTY_PRINT);
        return response($queen_bee, 200);
       
    }

    public function getcode($company)
    {
        $codenum = DB::table('mst_hives')->select('code')->where('company', $company)->count();
        $codenum = $codenum + 1;
        $code_generate = str_pad($codenum,4,"0",STR_PAD_LEFT);
        return $code_generate;
       
    }
    public function edit($id,$company)
    {
        //
    }

    public function update(Request $request, $company,$id)
    {
        if (m_hives::where('id', $id)->exists()) {
            $queen_bee = m_hives::find($id);
            $queen_bee->code = is_null($request->code) ? $queen_bee->code : $request->code;
            $queen_bee->state = is_null($request->state) ? $queen_bee->state : $request->state;
            $queen_bee->company = is_null($request->company) ? $queen_bee->company : $request->company;
            $queen_bee->type_hives = is_null($request->type_hives) ? $queen_bee->type_hives : $request->type_hives;
            $queen_bee->origin_hives = is_null($request->origin_hives) ? $queen_bee->origin_hives : $request->origin_hives;
            $queen_bee->apiaries = is_null($request->apiaries) ? $queen_bee->apiaries : $request->apiaries;
            $queen_bee->queen_race = is_null($request->queen_race) ? $queen_bee->queen_race : $request->queen_race;
            $queen_bee->queen_id = is_null($request->queen_id) ? $queen_bee->queen_id : $request->queen_id;
            $queen_bee->date_installed = is_null($request->date_installed) ? $queen_bee->date_installed : $request->date_installed;
            $queen_bee->rice = is_null($request->rice) ? $queen_bee->rice : $request->rice;
            $queen_bee->save();
            return response()->json([
                "message" => $this->m->update(),"color" => $this->m->success()
            ], 200);
        } else {
            return response()->json([
                "message" => $this->m->NotFound(),"color" => $this->m->error()
            ], 404);
            
        }
    }

    public function destroy($company,$id)
    {
        if(m_hives::where('id', $id)->exists()) {
            $queen_bee = m_hives::find($id);
            $queen_bee->delete();

            return response()->json([
              "message" => $this->m->delete(),"color" => $this->m->success()
            ], 202);
          } else {
            return response()->json([
              "message" => $this->m->NotFound(),"color" => $this->m->error()
            ], 404);
          }
    }
}
