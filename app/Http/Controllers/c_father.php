<?php

namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\model\m_father;
use App\model\m_father_mother;
use App\response\message;
use Illuminate\Support\Facades\DB;

class c_father extends Controller
{
    private $m;

    public function __construct()
    {
        $this->m = new message;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($company)
    {
        $bee_father = DB::table('mst_bees_father')
            ->join('mst_hives_origin','mst_bees_father.origin','=','mst_hives_origin.code')
            ->join('mst_queen_race','mst_bees_father.race','=','mst_queen_race.code')
            ->select('mst_bees_father.*', 'mst_hives_origin.name AS name_origin', 'mst_queen_race.name AS name_race')
            ->where('mst_bees_father.company', $company)
            ->get()
            ->toJson(JSON_PRETTY_PRINT);
        return response($bee_father, 200);

    }



    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request,$company)m_father
    {
        //
        if(m_father::where('code', $request->code)->exists()){
            return response()->json(["message" => $this->m->duplicate(),"color" => $this->m->error()], 201);
        }
        $father = "";
        foreach ($request->father as $value) {
            if($father == ""){
              $father = $father.",".$value;
            }
            else{
              $father = $father.",".$value;
            }
          }
        $bee_father = new m_father;
        $bee_father->code = $request->code;
        $bee_father->source = $request->source;
        $bee_father->supplier = $request->supplier;
        $bee_father->origin = $request->origin;
        $bee_father->race = $request->race;
        $bee_father->mother = $request->mother;
        $bee_father->father = $father;
        $bee_father->insertion_date = $request->insertion_date;
        $bee_father->company = $request->company;
        $bee_father->state = $request->state;
        $bee_father->save();
        $list = new m_father_mother;
            $list->father =  $request->code;
            $list->mother = $request->mother;
            $list->save();
        foreach ($request->motherlist as $value) {
            $v = "";
            $v = $value["mother"];
            $list = new m_father_mother;
            $list->father =  $request->code;
            $list->mother = $request->mother."-".$v;
            $list->save();
        }
        echo $request;
        return response()->json([
          "message" =>  $this->m->create(),"color" => $this->m->success()
        ], 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($company,$id)
    {
        //
        if (m_father::where('id', $id)->exists()) {
            $bee_father = m_father::where('id', $id)->get()->toJson(JSON_PRETTY_PRINT);
            return response($bee_father, 200);
        } else {
            return response()->json([
              "message" => $this->m->NotFound(),"color" => $this->m->error()
            ], 404);
        }
        return m_father::where('id',$id)->get();
    }

    public function show_mother($company,$father)
    {
        //
        if (m_father_mother::where('father', $father)->exists()) {
            $bee_father = m_father_mother::where('father', $father)->get()->toJson(JSON_PRETTY_PRINT);
            return response($bee_father, 200);
        } else {
            return response()->json([
              "message" => $this->m->NotFound(),"color" => $this->m->error()
            ], 404);
        }
        return m_father_mother::where('father',$father)->get();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function combobox($company)
    {
        $bee_father = m_father::where('company', $company)->where('state', true)->select('code')->get()->toJson(JSON_PRETTY_PRINT);
        return response($bee_father, 200);
       
    }
    public function getcode($company)
    {
        $codenum = DB::table('mst_bees_father')->select('code')->where('company', $company)->count();
        $codenum = $codenum + 1;
        $code_generate = 'P'.str_pad($codenum,5,"0",STR_PAD_LEFT);
        return $code_generate;
       
    }
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$company, $id)
    {
        //
        if (m_father::where('id', $id)->exists()) {
            $bee_father = m_father::find($id);
            $bee_father->code = is_null($request->code) ? $bee_father->code : $request->code;
            $bee_father->source = is_null($request->source) ? $bee_father->source : $request->source;
            $bee_father->supplier = is_null($request->supplier) ? $bee_father->supplier : $request->supplier;
            $bee_father->state = is_null($request->state) ? $bee_father->state : $request->state;
            $bee_father->origin = is_null($request->origin) ? $bee_father->origin : $request->origin;
            $bee_father->race = is_null($request->race) ? $bee_father->race : $request->race;
            $bee_father->mother = is_null($request->mother) ? $bee_father->mother : $request->mother;
            $bee_father->father = is_null($request->father) ? $bee_father->father : $request->father;
            $bee_father->insertion_date = is_null($request->insertion_date) ? $bee_father->insertion_date : $request->insertion_date;
            $bee_father->company = is_null($request->company) ? $bee_father->company : $request->company;
            $bee_father->save();
            return response()->json([
                "message" => $this->m->update(),"color" => $this->m->success()
            ], 200);
        } else {
            return response()->json([
                "message" => $this->m->NotFound(),"color" => $this->m->error()
            ], 404);
            
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        if(m_father::where('id', $id)->exists()) {
            $bee_father = m_father::find($id);
            $bee_father->delete();

            return response()->json([
                "message" => $this->m->delete(),"color" => $this->m->success()
            ], 202);
        } else {
            return response()->json([
                "message" => $this->m->NotFound(),"color" => $this->m->error()
            ], 404);
        }
    }
}
