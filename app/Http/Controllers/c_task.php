<?php

namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\model\m_task;
use App\response\message;

class c_task extends Controller
{
    private $m;

    public function __construct()
    {
        $this->m = new message;
    }


    public function index($company)
    {
        $task = m_task::where('company', $company)->get()->toJson(JSON_PRETTY_PRINT);
        return response($task, 200);
    }
    public function getbyproblem($company,$code)
    {
        $task = m_task::where('company', $company)->where('problem', $code)->get()->toJson(JSON_PRETTY_PRINT);
        return response($task, 200);
    }
    public function create()
    {

    }

    public function store(Request $request,$company)
    {
        
        if(m_task::where('code', $request->code)->exists()){
            return response()->json(["message" => $this->m->duplicate(),"color" => $this->m->error()], 201);
        }
        $task = new m_task;
        $task->code = $request->code;
        $task->description = $request->description;
        $task->company = $request->company;
        $task->problem = $request->problem;
        $task->state = $request->state;
        $task->save();
        return response()->json([
          "message" =>  $this->m->create(),"color" => $this->m->success()
        ], 201);
    }

    public function show($company,$id)
    {
        if (m_task::where('id', $id)->exists()) {
            $task = m_task::where('id', $id)->get()->toJson(JSON_PRETTY_PRINT);
            return response($task, 200);
        } else {
            return response()->json([
              "message" => $this->m->NotFound(),"color" => $this->m->error()
            ], 404);
        }
        return m_task::where('id',$id)->get();
    }
    public function combobox($company)
    {
        $task = m_task::where('company', $company)->where('state', true)->select('code','description')->get()->toJson(JSON_PRETTY_PRINT);
        return response($task, 200);
       
    }
    public function edit($id,$company)
    {
        //
    }

    public function update(Request $request, $company,$id)
    {
        if (m_task::where('id', $id)->exists()) {
            $task = m_task::find($id);
            $task->code = is_null($request->code) ? $task->code : $request->code;
            $task->description = is_null($request->description) ? $task->description : $request->description;
            $task->state = is_null($request->state) ? $task->state : $request->state;
            $task->company = is_null($request->company) ? $task->company : $request->company;
            $task->problem = is_null($request->problem) ? $task->problem : $request->problem;
            $task->save();
            return response()->json([
                "message" => $this->m->update(),"color" => $this->m->success()
            ], 200);
        } else {
            return response()->json([
                "message" => $this->m->NotFound(),"color" => $this->m->error()
            ], 404);
            
        }
    }

    public function destroy($company,$id)
    {
        if(m_task::where('id', $id)->exists()) {
            $task = m_task::find($id);
            $task->delete();

            return response()->json([
              "message" => $this->m->delete(),"color" => $this->m->success()
            ], 202);
          } else {
            return response()->json([
              "message" => $this->m->NotFound(),"color" => $this->m->error()
            ], 404);
          }
    }
}
