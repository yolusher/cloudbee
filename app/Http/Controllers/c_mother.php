<?php

namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\model\m_mother;
use App\response\message;
use Illuminate\Support\Facades\DB;

class c_mother extends Controller
{
    private $m;

    public function __construct()
    {
        $this->m = new message;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($company)
    {
        //
        /*$supplier = DB::table('mst_bees_supplier')
            ->join('mst_hives_origin', 'mst_bees_supplier.origin', '=', 'mst_hives_origin.code')
            ->select('mst_bees_supplier.*', 'mst_hives_origin.name AS name_origin')
            ->where('mst_bees_supplier.company', $company)
            ->get()
            ->toJson(JSON_PRETTY_PRINT);race
        return response($supplier, 200);*/
        /*$bee_mother = m_mother::where('company', $company)->get()->toJson(JSON_PRETTY_PRINT);
        return response($bee_mother, 200);*/
         
        $bee_mother = DB::table('mst_bees_mother')
            ->join('mst_hives_origin','mst_bees_mother.origin','=','mst_hives_origin.code')
            ->join('mst_queen_race','mst_bees_mother.race','=','mst_queen_race.code')
            ->select('mst_bees_mother.*', 'mst_hives_origin.name AS name_origin', 'mst_queen_race.name AS name_race')
            ->where('mst_bees_mother.company', $company)
            ->get()
            ->toJson(JSON_PRETTY_PRINT);
        return response($bee_mother, 200);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request,$company)
    {
        //
        if(m_mother::where('code', $request->code)->exists()){
            return response()->json(["message" => $this->m->duplicate(),"color" => $this->m->error()], 201);
        }

        $bee_mother = new m_mother;
        $bee_mother->code = $request->code;
        $bee_mother->source = $request->source;
        $bee_mother->supplier = $request->supplier;
        $bee_mother->origin = $request->origin;
        $bee_mother->race = $request->race;
        $bee_mother->mother = $request->mother;
        $bee_mother->father = $request->father;
        $bee_mother->insertion_date = $request->insertion_date;
        $bee_mother->company = $request->company;
        $bee_mother->state = $request->state;
        $bee_mother->save();
        return response()->json([
          "message" =>  $this->m->create(),"color" => $this->m->success()
        ], 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($company,$id)
    {
        //
        if (m_mother::where('id', $id)->exists()) {
            $bee_mother = m_mother::where('id', $id)->get()->toJson(JSON_PRETTY_PRINT);
            return response($bee_mother, 200);
        } else {
            return response()->json([
              "message" => $this->m->NotFound(),"color" => $this->m->error()
            ], 404);
        }
        return m_mother::where('id',$id)->get();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function combobox($company)
    {
        $bee_mother = m_mother::where('company', $company)->where('state', true)->select('code')->get()->toJson(JSON_PRETTY_PRINT);
        return response($bee_mother, 200);
       
    }
    public function getcode($company)
    {
        $codenum = DB::table('mst_bees_mother')->select('code')->where('company', $company)->count();
        $codenum = $codenum + 1;
        $code_generate = 'C'.str_pad($codenum,5,"0",STR_PAD_LEFT);
        return $code_generate;
       
    }
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$company, $id)
    {
        //
        if (m_mother::where('id', $id)->exists()) {
            $bee_mother = m_mother::find($id);
            $bee_mother->code = is_null($request->code) ? $bee_mother->code : $request->code;
            $bee_mother->source = is_null($request->source) ? $bee_mother->source : $request->source;
            $bee_mother->supplier = is_null($request->supplier) ? $bee_mother->supplier : $request->supplier;
            $bee_mother->state = is_null($request->state) ? $bee_mother->state : $request->state;
            $bee_mother->origin = is_null($request->origin) ? $bee_mother->origin : $request->origin;
            $bee_mother->race = is_null($request->race) ? $bee_mother->race : $request->race;
            $bee_mother->mother = is_null($request->mother) ? $bee_mother->mother : $request->mother;
            $bee_mother->father = is_null($request->father) ? $bee_mother->father : $request->father;
            $bee_mother->insertion_date = is_null($request->insertion_date) ? $bee_mother->insertion_date : $request->insertion_date;
            $bee_mother->company = is_null($request->company) ? $bee_mother->company : $request->company;
            $bee_mother->save();
            return response()->json([
                "message" => $this->m->update(),"color" => $this->m->success()
            ], 200);
        } else {
            return response()->json([
                "message" => $this->m->NotFound(),"color" => $this->m->error()
            ], 404);
            
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        if(m_mother::where('id', $id)->exists()) {
            $bee_mother = m_mother::find($id);
            $bee_mother->delete();

            return response()->json([
                "message" => $this->m->delete(),"color" => $this->m->success()
            ], 202);
        } else {
            return response()->json([
                "message" => $this->m->NotFound(),"color" => $this->m->error()
            ], 404);
        }
    }
}
