<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\model\m_illness;
use App\response\message;

class c_illness extends Controller
{
    private $m;

    public function __construct()
    {
        $this->m = new message;
    }


    public function index($company)
    {
        $illness = m_illness::where('company', $company)->get()->toJson(JSON_PRETTY_PRINT);
        return response($illness, 200);
    }

    
    public function create()
    {

    }

    public function store(Request $request,$company)
    {
        
        if(m_illness::where('code', $request->code)->exists()){
            return response()->json(["message" => $this->m->duplicate(),"color" => $this->m->error()], 201);
        }
        $illness = new m_illness;
        $illness->code = $request->code;
        $illness->description = $request->description;
        $illness->company = $request->company;
        $illness->state = $request->state;
        $illness->save();
        return response()->json([
          "message" =>  $this->m->create(),"color" => $this->m->success()
        ], 201);
    }

    public function show($company,$id)
    {
        if (m_illness::where('id', $id)->exists()) {
            $illness = m_illness::where('id', $id)->get()->toJson(JSON_PRETTY_PRINT);
            return response($illness, 200);
        } else {
            return response()->json([
              "message" => $this->m->NotFound(),"color" => $this->m->error()
            ], 404);
        }
        return m_illness::where('id',$id)->get();
    }
    public function combobox($company)
    {
        $illness = m_illness::where('company', $company)->where('state', true)->select('code','name')->get()->toJson(JSON_PRETTY_PRINT);
        return response($illness, 200);
       
    }
    public function edit($id,$company)
    {
        //
    }

    public function update(Request $request, $company,$id)
    {
        if (m_illness::where('id', $id)->exists()) {
            $illness = m_illness::find($id);
            $illness->code = is_null($request->code) ? $illness->code : $request->code;
            $illness->description = is_null($request->description) ? $illness->description : $request->description;
            $illness->state = is_null($request->state) ? $illness->state : $request->state;
            $illness->company = is_null($request->company) ? $illness->company : $request->company;
            $illness->save();
            return response()->json([
                "message" => $this->m->update(),"color" => $this->m->success()
            ], 200);
        } else {
            return response()->json([
                "message" => $this->m->NotFound(),"color" => $this->m->error()
            ], 404);
            
        }
    }

    public function destroy($company,$id)
    {
        if(m_illness::where('id', $id)->exists()) {
            $illness = m_illness::find($id);
            $illness->delete();

            return response()->json([
              "message" => $this->m->delete(),"color" => $this->m->success()
            ], 202);
          } else {
            return response()->json([
              "message" => $this->m->NotFound(),"color" => $this->m->error()
            ], 404);
          }
    }
}
