<?php

namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\model\m_userapp;
use App\response\message;

class c_userapp extends Controller
{
    private $m;

    public function __construct()
    {
        $this->m = new message;
    }


    public function index($company)
    {
        $userapp = m_userapp::where('company', $company)->get()->toJson(JSON_PRETTY_PRINT);
        return response($userapp, 200);
    }

    
    public function login(Request $request)
    {
    	
    	 if (m_userapp::where('username', $request->username)->where('password', $request->password)->exists()) {
            $userapp =m_userapp::where('username', $request->username)->where('password', $request->password)->limit(1)->get()->toJson(JSON_PRETTY_PRINT);
            return response($userapp, 200);
        } else {
            return response()->json([
              "message" => $this->m->NotFound(),"color" => $this->m->error()
            ], 404);
        }
    }

    public function store(Request $request,$company)
    {
        
        if(m_userapp::where('id', $request->id)->exists()){
            return response()->json(["message" => $this->m->duplicate(),"color" => $this->m->error()], 201);
        }
        $userapp = new m_userapp;
        $userapp->name = $request->name;
        $userapp->username = $request->username;
        $userapp->email = $request->email;
        $userapp->password = $request->password;
        $userapp->company = $request->company;
        $userapp->active = $request->active;
        $userapp->save();
        return response()->json([
          "message" =>  $this->m->create(),"color" => $this->m->success()
        ], 201);
    }

    public function show($company,$id)
    {
        if (m_userapp::where('id', $id)->exists()) {
            $userapp = m_userapp::where('id', $id)->get()->toJson(JSON_PRETTY_PRINT);
            return response($userapp, 200);
        } else {
            return response()->json([
              "message" => $this->m->NotFound(),"color" => $this->m->error()
            ], 404);
        }
        return m_userapp::where('id',$id)->get();
    }
    public function combobox($company)
    {
        $userapp = m_userapp::where('company', $company)->where('state', true)->select('id','name')->get()->toJson(JSON_PRETTY_PRINT);
        return response($userapp, 200);
       
    }
    public function edit($id,$company)
    {
        //
    }

    public function update(Request $request, $company,$id)
    {
        if (m_userapp::where('id', $id)->exists()) {
            $userapp = m_userapp::find($id);
            $userapp->id = is_null($request->id) ? $userapp->id : $request->id;
            $userapp->name = is_null($request->name) ? $userapp->name : $request->name;
            $userapp->username = is_null($request->username) ? $userapp->username : $request->username;
            $userapp->email = is_null($request->email) ? $userapp->email : $request->email;
            $userapp->password = is_null($request->password) ? $userapp->password : $request->password;
            $userapp->company = is_null($request->company) ? $userapp->company : $request->company;
            $userapp->active = is_null($request->active) ? $userapp->active : $request->active;
            $userapp->save();
            return response()->json([
                "message" => $this->m->update(),"color" => $this->m->success()
            ], 200);
        } else {
            return response()->json([
                "message" => $this->m->NotFound(),"color" => $this->m->error()
            ], 404);
            
        }
    }

    public function destroy($company,$id)
    {
        if(m_userapp::where('id', $id)->exists()) {
            $userapp = m_userapp::find($id);
            $userapp->delete();

            return response()->json([
              "message" => $this->m->delete(),"color" => $this->m->success()
            ], 202);
          } else {
            return response()->json([
              "message" => $this->m->NotFound(),"color" => $this->m->error()
            ], 404);
          }
    }
}
