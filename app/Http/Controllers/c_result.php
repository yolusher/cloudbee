<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\model\m_result;
use App\response\message;

class c_result extends Controller
{
    private $m;

    public function __construct()
    {
        $this->m = new message;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($company)
    {
        //
        $result = m_result::where('company', $company)->get()->toJson(JSON_PRETTY_PRINT);
        return response($result, 200);
    }

    public function report(Request $request,$company)
    {
        //
        $result = m_result::where('company', $company)->where('date', '>',  $request->dateini)
            ->where('date', '<',  $request->dateend)->get()->toJson(JSON_PRETTY_PRINT);
        return response($result, 200);
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function get_cb($company,$hives)
    {
        $result = m_result::where('company', $company)->where('hives', $hives)->get()->toJson(JSON_PRETTY_PRINT);
        return response($result, 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }



    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request,$company)
    {
        //
        $result = new m_result;
        $result->user = $request->user;
        $result->code = $request->code;
        $result->company = $company;
        $result->date = $request->date;
        $result->hour = $request->hour;
        $result->hives = $request->hives;
        $result->result = json_encode($request->response);
        $result->save();
        return response()->json([
          "message" =>  $this->m->create(),"color" => $this->m->success()
        ], 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($company,$id)
    {
        //
        if (m_result::where('id', $id)->exists()) {
            $result = m_result::where('id', $id)->where('company', $company)->get()->toJson(JSON_PRETTY_PRINT);
            return response($result, 200);
        } else {
            return response()->json([
              "message" => $this->m->NotFound(),"color" => $this->m->error()
            ], 404);
        }
        return m_result::where('id',$id)->where('company',$company)->get();
    }
    
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$company,$id)
    {

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($company,$id)
    {
        //
    }
}
