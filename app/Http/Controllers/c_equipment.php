<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\model\m_equipment;
use App\response\message;

class c_equipment extends Controller
{
    private $m;

    public function __construct()
    {
        $this->m = new message;
    }


    public function index($company)
    {
        $equipment = m_equipment::where('company', $company)->get()->toJson(JSON_PRETTY_PRINT);
        return response($equipment, 200);
    }

    
    public function create()
    {

    }

    public function store(Request $request,$company)
    {
        

        $equipment = new m_equipment;
        $equipment->code = $request->code;
        $equipment->name = $request->name;
        $equipment->company = $request->company;
        $equipment->state = $request->state;
        $equipment->_number = $request->_number;
        $equipment->honey = $request->honey;
        $equipment->breeding = $request->breeding;
        $equipment->save();
        return response()->json([
          "message" =>  $this->m->create(),"color" => $this->m->success()
        ], 201);
    }
    public function showequipment($company,$hives)
    {
        $equipment = m_equipment::where('company', $company)->where('code', $hives)->get()->toJson(JSON_PRETTY_PRINT);
        return response($equipment, 200);
    }

    public function show($company,$id)
    {
        if (m_equipment::where('id', $id)->exists()) {
            $equipment = m_equipment::where('id', $id)->get()->toJson(JSON_PRETTY_PRINT);
            return response($equipment, 200);
        } else {
            return response()->json([
              "message" => $this->m->NotFound(),"color" => $this->m->error()
            ], 404);
        }
        return m_equipment::where('id',$id)->get();
    }
    public function combobox($company)
    {
        $equipment = m_equipment::where('company', $company)->where('state', true)->select('code','name')->get()->toJson(JSON_PRETTY_PRINT);
        return response($equipment, 200);
       
    }
    public function edit($id,$company)
    {
        //
    }

    public function update(Request $request, $company,$id)
    {
        if (m_equipment::where('id', $id)->exists()) {
            $equipment = m_equipment::find($id);
            $equipment->code = is_null($request->code) ? $equipment->code : $request->code;
            $equipment->name = is_null($request->name) ? $equipment->name : $request->name;
            $equipment->state = is_null($request->state) ? $equipment->state : $request->state;
            $equipment->_number = is_null($request->_number) ? $equipment->_number : $request->_number;
            $equipment->honey = is_null($request->honey) ? $equipment->honey : $request->honey;
            $equipment->breeding = is_null($request->breeding) ? $equipment->breeding : $request->breeding;
            $equipment->company = is_null($request->company) ? $equipment->company : $request->company;
            $equipment->save();
            return response()->json([
                "message" => $this->m->update(),"color" => $this->m->success()
            ], 200);
        } else {
            return response()->json([
                "message" => $this->m->NotFound(),"color" => $this->m->error()
            ], 404);
            
        }
    }

    public function destroy($company,$id)
    {
        if(m_equipment::where('id', $id)->exists()) {
            $equipment = m_equipment::find($id);
            $equipment->delete();

            return response()->json([
              "message" => $this->m->delete(),"color" => $this->m->success()
            ], 202);
          } else {
            return response()->json([
              "message" => $this->m->NotFound(),"color" => $this->m->error()
            ], 404);
          }
    }
}
