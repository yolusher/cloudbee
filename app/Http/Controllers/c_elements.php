<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\model\m_elements;
use App\response\message;

class c_elements extends Controller
{
    private $m;

    public function __construct()
    {
        $this->m = new message;
    }


    public function index($company)
    {
        $elements = m_elements::where('company', $company)->get()->toJson(JSON_PRETTY_PRINT);
        return response($elements, 200);
    }

    
    public function create()
    {

    }

    public function store(Request $request,$company)
    {
        
        if(m_elements::where('code', $request->code)->exists()){
            return response()->json(["message" => $this->m->duplicate(),"color" => $this->m->error()], 201);
        }
        $elements = new m_elements;
        $elements->code = $request->code;
        $elements->name = $request->name;
        $elements->unit_measure = $request->unit_measure;
        $elements->company = $request->company;
        $elements->state = $request->state;
        $elements->save();
        return response()->json([
          "message" =>  $this->m->create(),"color" => $this->m->success()
        ], 201);
    }

    public function show($company,$id)
    {
        if (m_elements::where('id', $id)->exists()) {
            $elements = m_elements::where('id', $id)->get()->toJson(JSON_PRETTY_PRINT);
            return response($elements, 200);
        } else {
            return response()->json([
              "message" => $this->m->NotFound(),"color" => $this->m->error()
            ], 404);
        }
        return m_elements::where('id',$id)->get();
    }
    public function combobox($company)
    {
        $elements = m_elements::where('company', $company)->where('state', true)->select('code','name')->get()->toJson(JSON_PRETTY_PRINT);
        return response($elements, 200);
       
    }
    public function edit($id,$company)
    {
        //
    }

    public function update(Request $request, $company,$id)
    {
        if (m_elements::where('id', $id)->exists()) {
            $elements = m_elements::find($id);
            $elements->code = is_null($request->code) ? $elements->code : $request->code;
            $elements->name = is_null($request->name) ? $elements->name : $request->name;
            $elements->unit_measure = is_null($request->unit_measure) ? $elements->unit_measure : $request->unit_measure;
            $elements->state = is_null($request->state) ? $elements->state : $request->state;
            $elements->company = is_null($request->company) ? $elements->company : $request->company;
            $elements->save();
            return response()->json([
                "message" => $this->m->update(),"color" => $this->m->success()
            ], 200);
        } else {
            return response()->json([
                "message" => $this->m->NotFound(),"color" => $this->m->error()
            ], 404);
            
        }
    }

    public function destroy($company,$id)
    {
        if(m_elements::where('id', $id)->exists()) {
            $elements = m_elements::find($id);
            $elements->delete();

            return response()->json([
              "message" => $this->m->delete(),"color" => $this->m->success()
            ], 202);
          } else {
            return response()->json([
              "message" => $this->m->NotFound(),"color" => $this->m->error()
            ], 404);
          }
    }
}
