<?php

namespace App\Http\Controllers;
use App\model\m_image_payment;

use Illuminate\Http\Request;

class f_paymentfile extends Controller
{
    public function getfile($company,$name){
    	//return response()->download(public_path('images/'+$name))
    	//return response()->download(public_path('images/payment/prueba.jpg'),'Prueba');
    	$photoURL = url('/images/payment/'.$name);
    	return response()->json(['url' => $photoURL],200);
    }

    public function savefile(Request $request){
    	
    	$filename = "pago_".rand(1000000000,10000000000).".jpg";
    	$image = new m_image_payment(['name'=>$filename ,'company'=>$request->company]);
        $image->save();
    	$path = $request->file('photo')->move(public_path('/images/payment/'),$filename);
    	$photoURL = url('/images/payment/'.$filename);
    	return response()->json([
                "message" => "Compañia Registrada. Recibira un correo de confirmación","color" => "success"
            ], 200);
    }
}
