<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\model\m_meekness;
use App\response\message;

class c_meekness extends Controller
{
    private $m;

    public function __construct()
    {
        $this->m = new message;
    }


    public function index($company)
    {
        $meekness = m_meekness::where('company', $company)->get()->toJson(JSON_PRETTY_PRINT);
        return response($meekness, 200);
    }

    
    public function create()
    {

    }

    public function store(Request $request,$company)
    {
        
        if(m_meekness::where('code', $request->code)->exists()){
            return response()->json(["message" => $this->m->duplicate(),"color" => $this->m->error()], 201);
        }
        $meekness = new m_meekness;
        $meekness->code = $request->code;
        $meekness->name = $request->name;
        $meekness->calification = $request->calification;
        /*$meekness->storage = $request->storage;
        $meekness->attitude = $request->attitude;
        $meekness->meekness = $request->meekness;
        $meekness->production = $request->production;*/
        $meekness->company = $request->company;
        $meekness->state = $request->state;
        $meekness->save();
        return response()->json([
          "message" =>  $this->m->create(),"color" => $this->m->success()
        ], 201);
    }

    public function show($company,$id)
    {
        if (m_meekness::where('id', $id)->exists()) {
            $meekness = m_meekness::where('id', $id)->get()->toJson(JSON_PRETTY_PRINT);
            return response($meekness, 200);
        } else {
            return response()->json([
              "message" => $this->m->NotFound(),"color" => $this->m->error()
            ], 404);
        }
        return m_meekness::where('id',$id)->get();
    }
    public function combobox($company)
    {
        $meekness = m_meekness::where('company', $company)->where('state', true)->select('code','name')->get()->toJson(JSON_PRETTY_PRINT);
        return response($meekness, 200);
       
    }
    public function edit($id,$company)
    {
        //
    }

    public function update(Request $request, $company,$id)
    {
        if (m_meekness::where('id', $id)->exists()) {
            $meekness = m_meekness::find($id);
            $meekness->code = is_null($request->code) ? $meekness->code : $request->code;
            $meekness->name = is_null($request->name) ? $meekness->name : $request->name;
            $meekness->calification = is_null($request->calification) ? $meekness->calification : $request->calification;
            /*$meekness->attitude = is_null($request->attitude) ? $meekness->attitude : $request->attitude;
            $meekness->meekness = is_null($request->meekness) ? $meekness->meekness : $request->meekness;
            $meekness->production = is_null($request->production) ? $meekness->production : $request->production;
            $meekness->storage = is_null($request->storage) ? $meekness->storage : $request->storage;*/
            $meekness->state = is_null($request->state) ? $meekness->state : $request->state;
            $meekness->company = is_null($request->company) ? $meekness->company : $request->company;
            $meekness->save();
            return response()->json([
                "message" => $this->m->update(),"color" => $this->m->success()
            ], 200);
        } else {
            return response()->json([
                "message" => $this->m->NotFound(),"color" => $this->m->error()
            ], 404);
            
        }
    }

    public function destroy($company,$id)
    {
        if(m_meekness::where('id', $id)->exists()) {
            $meekness = m_meekness::find($id);
            $meekness->delete();

            return response()->json([
              "message" => $this->m->delete(),"color" => $this->m->success()
            ], 202);
          } else {
            return response()->json([
              "message" => $this->m->NotFound(),"color" => $this->m->error()
            ], 404);
          }
    }
}
