<?php

namespace App\model;

use Illuminate\Database\Eloquent\Model;

class m_storage extends Model
{
       
    public $timestamps = false;
    protected $table ='mst_storage';
	protected $primaryKey = 'id';
	protected $fillable = ['code','name','company','state'];
}
