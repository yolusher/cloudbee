<?php

namespace App\model;

use Illuminate\Database\Eloquent\Model;

class m_queen_race extends Model
{
    public $timestamps = false;
    protected $table ='mst_queen_race';
	protected $primaryKey = 'id';
	protected $fillable = ['code','name','company','state'];
}
