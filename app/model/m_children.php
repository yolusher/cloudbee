<?php

namespace App\model;

use Illuminate\Database\Eloquent\Model;

class m_children extends Model
{
    //
    public $timestamps = false;
    protected $table ='mst_bees_children';
	protected $primaryKey = 'id';
	protected $fillable = ['code','mother','father','type_bee','insertion_date','status','company','state'];
}
