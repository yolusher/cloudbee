<?php

namespace App\model;

use Illuminate\Database\Eloquent\Model;

class m_supplier extends Model
{
    //
    public $timestamps = false;
    protected $table ='mst_bees_supplier';
	protected $primaryKey = 'id';
	protected $fillable = ['code','name','origin','company','state'];
}
