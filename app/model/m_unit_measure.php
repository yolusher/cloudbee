<?php

namespace App\model;

use Illuminate\Database\Eloquent\Model;

class m_unit_measure extends Model
{
    public $timestamps = false;
    protected $table ='conf_unit_measure';
	protected $primaryKey = 'id';
	protected $fillable = ['code','name','sym','company','state'];
}
