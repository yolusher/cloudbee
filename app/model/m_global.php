<?php

namespace App\model;

use Illuminate\Database\Eloquent\Model;

class m_global extends Model
{
    public $timestamps = false;
    protected $table ='conf_global';
	protected $primaryKey = 'id';
	protected $fillable = ['code','name','company','_type','unit','state'];
}
