<?php

namespace App\model;

use Illuminate\Database\Eloquent\Model;

class m_company extends Model
{
    public $timestamps = false;
    protected $table ='conf_company';
	protected $primaryKey = 'id';
	protected $fillable = ['code','name','plan','state'];
}
