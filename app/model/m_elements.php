<?php

namespace App\model;

use Illuminate\Database\Eloquent\Model;

class m_elements extends Model
{
    public $timestamps = false;
    protected $table ='mst_elements';
	protected $primaryKey = 'id';
	protected $fillable = ['code','name','unit_measure','company','state'];
}
