<?php

namespace App\model;

use Illuminate\Database\Eloquent\Model;

class m_father extends Model
{
    //
    public $timestamps = false;
    protected $table ='mst_bees_father';
	protected $primaryKey = 'id';
	protected $fillable = ['code','source','supplier','origin','race','mother','father','insertion_date','company','state'];
}
