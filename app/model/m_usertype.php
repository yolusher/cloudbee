<?php

namespace App\model;

use Illuminate\Database\Eloquent\Model;

class m_usertype extends Model
{
	public $timestamps = false;
    protected $table ='conf_type_user';
	protected $primaryKey = 'id';
	protected $fillable = ['code','name','active'];
}
