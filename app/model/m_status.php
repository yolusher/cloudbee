<?php

namespace App\model;

use Illuminate\Database\Eloquent\Model;

class m_status extends Model
{
    //
    public $timestamps = false;
    protected $table ='mst_bee_status';
	protected $primaryKey = 'id';
	protected $fillable = ['code','name','company','state'];

}
