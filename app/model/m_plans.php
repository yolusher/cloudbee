<?php

namespace App\model;

use Illuminate\Database\Eloquent\Model;

class m_plans extends Model
{
    public $timestamps = false;
    protected $table ='conf_company_plans';
	protected $primaryKey = 'id';
	protected $fillable = ['code','name','max_hives','state'];
}
