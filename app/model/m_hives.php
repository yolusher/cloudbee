<?php

namespace App\model;

use Illuminate\Database\Eloquent\Model;

class m_hives extends Model
{
    public $timestamps = false;
    protected $table ='mst_hives';
	protected $primaryKey = 'id';
	protected $fillable = ['code','type_hives','origin_hives','apiaries','queen_race','queen_id','date_installed','rice','company','state'];
}

