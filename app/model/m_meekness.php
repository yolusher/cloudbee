<?php

namespace App\model;

use Illuminate\Database\Eloquent\Model;

class m_meekness extends Model
{
       public $timestamps = false;
    protected $table ='mst_meekness';
	protected $primaryKey = 'id';
	protected $fillable = ['code','name','company','storage','attitude','meekness','production','state'];
}

