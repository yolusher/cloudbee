<?php

namespace App\model;

use Illuminate\Database\Eloquent\Model;

class m_problem extends Model
{
    public $timestamps = false;
    protected $table ='mst_problem';
	protected $primaryKey = 'id';
	protected $fillable = ['code','description','company','state'];
}
