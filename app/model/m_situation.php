<?php

namespace App\model;

use Illuminate\Database\Eloquent\Model;

class m_situation extends Model
{
    //
    public $timestamps = false;
    protected $table ='mst_bee_situation';
	protected $primaryKey = 'id';
	protected $fillable = ['code','name','company','state'];

}
