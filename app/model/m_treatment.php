<?php

namespace App\model;

use Illuminate\Database\Eloquent\Model;

class m_treatment extends Model
{
    public $timestamps = false;
    protected $table ='mst_treatment';
	protected $primaryKey = 'id';
	protected $fillable = ['code','description','company','illness','unit_measure','state'];
}
