<?php

namespace App\model;

use Illuminate\Database\Eloquent\Model;

class m_result extends Model
{
    public $timestamps = false;
    protected $table ='app_result';
	protected $primaryKey = 'id';
	protected $fillable = ['code','user','date','hour','company','hives','result'];
}
