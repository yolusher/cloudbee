<?php

namespace App\model;

use Illuminate\Database\Eloquent\Model;

class m_hives_type extends Model
{
    public $timestamps = false;
    protected $table ='mst_hives_type';
	protected $primaryKey = 'id';
	protected $fillable = ['code','name','company','state'];
}
