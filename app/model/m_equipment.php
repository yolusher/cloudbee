<?php

namespace App\model;

use Illuminate\Database\Eloquent\Model;

class m_equipment extends Model
{
    public $timestamps = false;
    protected $table ='mst_equipment';
	protected $primaryKey = 'id';
	protected $fillable = ['code','name','company','_number','honey','breeding','state'];
}
