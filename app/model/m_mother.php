<?php

namespace App\model;

use Illuminate\Database\Eloquent\Model;

class m_mother extends Model
{
    //code,source, supplier, origin, race, mother, father, insertion_date, company, state
    public $timestamps = false;
    protected $table ='mst_bees_mother';
	protected $primaryKey = 'id';
	protected $fillable = ['code','source','supplier','origin','race','mother','father','insertion_date','company','state'];
}
