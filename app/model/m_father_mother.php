<?php

namespace App\model;

use Illuminate\Database\Eloquent\Model;

class m_father_mother extends Model
{
    public $timestamps = false;
    protected $table ='mst_father_mother';
	protected $primaryKey = 'id';
	protected $fillable = ['father','mother'];
}
