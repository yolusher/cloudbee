<?php

namespace App\model;

use Illuminate\Database\Eloquent\Model;

class m_userapp extends Model
{
    public $timestamps = false;
    protected $table ='app_users';
	protected $primaryKey = 'id';
	protected $fillable = ['id','name','username','email','password','company','active'];
}
