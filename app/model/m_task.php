<?php

namespace App\model;

use Illuminate\Database\Eloquent\Model;

class m_task extends Model
{
    public $timestamps = false;
    protected $table ='mst_task';
	protected $primaryKey = 'id';
	protected $fillable = ['code','description','company','problem','state'];
}
