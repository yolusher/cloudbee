<?php

namespace App\model;

use Illuminate\Database\Eloquent\Model;

class m_calification extends Model
{
    //
    public $timestamps = false;
    protected $table ='mst_ev_calification';
	protected $primaryKey = 'id';
	protected $fillable = ['code','name','company','state'];

}
