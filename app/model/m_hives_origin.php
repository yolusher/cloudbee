<?php

namespace App\model;

use Illuminate\Database\Eloquent\Model;

class m_hives_origin extends Model
{
    public $timestamps = false;
    protected $table ='mst_hives_origin';
	protected $primaryKey = 'id';
	protected $fillable = ['code','name','company','state'];
}
