<?php

namespace App\model;

use Illuminate\Database\Eloquent\Model;

class m_illness extends Model
{
    public $timestamps = false;
    protected $table ='mst_illness';
	protected $primaryKey = 'id';
	protected $fillable = ['code','description','company','state'];
}
