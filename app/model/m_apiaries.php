<?php

namespace App\model;

use Illuminate\Database\Eloquent\Model;

class m_apiaries extends Model
{
    public $timestamps = false;
    protected $table ='mst_apiaries';
	protected $primaryKey = 'id';
	protected $fillable = ['code','name','address','description','latitude','longitude','company','state'];
}

