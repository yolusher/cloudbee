<?php

namespace App\model;

use Illuminate\Database\Eloquent\Model;

class m_image_payment extends Model
{
    public $timestamps = false;
    protected $table ='conf_company_payment';
	protected $primaryKey = 'id';
	protected $fillable = ['name','company'];
}
