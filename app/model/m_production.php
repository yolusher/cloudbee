<?php

namespace App\model;

use Illuminate\Database\Eloquent\Model;

class m_production extends Model
{
    public $timestamps = false;
    protected $table ='mst_production';
	protected $primaryKey = 'id';
	protected $fillable = ['code','name','unit_measure','company','state'];
}
