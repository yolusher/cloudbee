<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Result extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('app_result', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('user', 255);
            $table->string('company', 255);
            $table->string('date', 255);
            $table->string('hour', 255);
            $table->string('hives', 255);
            $table->text('result');
        });
        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('app_result');
    }
}
