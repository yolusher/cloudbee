<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class MasterTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
   public function up()
    {
        Schema::create('mst_queen_race', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('code', 255);
            $table->string('name',255);
            $table->char('company',10);
            $table->boolean('state');
        });
        Schema::create('mst_hives_origin', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('code', 255);
            $table->string('name',255);
            $table->char('company',10);
            $table->boolean('state');
        });
        Schema::create('mst_hives_type', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('code', 255);
            $table->string('name',255);
            $table->char('company',10);
            $table->boolean('state');
        });
        Schema::create('mst_hives', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('code', 255);
            $table->string('type_hives',255);
            $table->string('origin_hives',255);
            $table->string('apiaries',255);
            $table->string('queen_race',255);
            $table->string('queen_id',255);
            $table->string('date_installed',255);
            $table->string('rice',255);
            $table->char('company',10);
            $table->boolean('state');
        });
        Schema::create('mst_apiaries', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('code', 255);
            $table->string('name',255);
            $table->string('address', 255);
            $table->string('description',255);
            $table->string('latitude', 255);
            $table->string('longitude',255);
            $table->char('company',10);
            $table->boolean('state');
        });
    }

/**
     * Reverse the migrations.
     *
     * @return void
     */

    public function down()
    {
        Schema::dropIfExists('mst_queen_race');
        Schema::dropIfExists('mst_hives_origin');
        Schema::dropIfExists('mst_hives_type');
        Schema::dropIfExists('mst_hives');
        Schema::dropIfExists('mst_apiaries');
    }
}
