<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class MstConfiguration extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mst_storage', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('code', 255);
            $table->string('name',255);
            $table->char('company',10);
            $table->boolean('state');
        });
        Schema::create('mst_meekness', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('code', 255);
            $table->string('name',255);
            $table->char('company',10);
            $table->boolean('state');
        });
        Schema::create('conf_unit_measure', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('code', 255);
            $table->string('sym', 255);
            $table->string('name',255);
            $table->char('company',10);
            $table->boolean('state');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mst_storage');
        Schema::dropIfExists('mst_meekness');
        Schema::dropIfExists('conf_unit_measure');
        
    }
}
