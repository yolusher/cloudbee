<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class MasterBeesC extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //code,source, supplier, origin, race, mother, father, insertion_date, company, state
        Schema::create('mst_bees_mother', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('code', 255);
            $table->string('source',255);
            $table->string('supplier',255);
            $table->string('origin',255);
            $table->string('race',255);
            $table->string('mother',255);
            $table->string('father',255);
            $table->string('insertion_date',255);
            $table->char('company',10);
            $table->boolean('state');
        });
        Schema::create('mst_bees_father', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('code', 255);
            $table->string('source',255);
            $table->string('supplier',255);
            $table->string('origin',255);
            $table->string('race',255);
            $table->string('mother',255);
            $table->string('father',255);
            $table->string('insertion_date',255);
            $table->char('company',10);
            $table->boolean('state');
        });
        Schema::create('mst_bees_children', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('code', 255);
            $table->string('mother',255);
            $table->string('father',255);
            $table->string('type_bee',255);
            $table->string('insertion_date',255);
            $table->string('status', 255);
            $table->char('company',10);
            $table->boolean('state');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::dropIfExists('mst_bees_mother');
        Schema::dropIfExists('mst_bees_father');
        Schema::dropIfExists('mst_bees_children');
    }
}
