<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class MasterIllness extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mst_illness', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('code', 255);
            $table->string('description',255);
            $table->char('company',10);
            $table->boolean('state');
        });
        Schema::create('mst_problem', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('code', 255);
            $table->string('description',255);
            $table->char('company',10);
            $table->boolean('state');
        });
        Schema::create('mst_treatment', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('code', 255);
            $table->string('treatment_type', 255);
            $table->string('description',255);
            $table->char('company',10);
            $table->boolean('state');
        });
        Schema::create('mst_equipment', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('code', 255);
            $table->string('name',255);
            $table->boolean('_number');
            $table->boolean('honey');
            $table->boolean('breeding');
            $table->char('company',10);
            $table->boolean('state');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mst_illness');
        Schema::dropIfExists('mst_problem');
        Schema::dropIfExists('mst_treatment');
        Schema::dropIfExists('mst_equipment');
    }
}
